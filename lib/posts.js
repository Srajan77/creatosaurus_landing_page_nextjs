import GhostContentAPI from '@tryghost/content-api';
import axios from 'axios';


// Create API instance with site credentials
const api = new GhostContentAPI({
  url: 'https://ahijftavqd.creatosaurus.io',
  key: 'b9777c6e7f8da644dfe80e81aa',
  version: 'v5.0',
});


// get all posts for blog xml
export async function getAllBlogs() {
  let posts = await api.posts
    .browse({
      include: 'title',
      include: 'slug',
      include: 'tags',
      limit: "all",
    })
    .catch((err) => {
      console.error(err);
    });

  return posts;
}


export async function getSinglePost(postSlug) {
  return await api.posts
    .read({
      slug: postSlug,
      include: 'authors,tags',
    })
    .catch((err) => {
      console.error(err);
    });
}

export async function getPostsByTags(tags) {
  try {
    const response = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/v4/content/posts/?key=b9777c6e7f8da644dfe80e81aa&include=authors,tags&filter=tags:${tags}&limit:all`)
    return response.data.posts;
  } catch (error) {
    console.error(error);
  }
}

export async function getPostsByAuthor(tags) {
  try {
    const response = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/v4/content/posts/?key=b9777c6e7f8da644dfe80e81aa&include=authors,tags&filter=author:${tags}&limit:all`)
    return response.data.posts;
  } catch (error) {
    console.error(error);
  }
}

