import axios from 'axios';

export async function getAuthorQuotes(search) {
    const response = await axios.get(`https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/search?q=${search}`)
    return response.data
}