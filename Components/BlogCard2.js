import React from 'react';
import BlogStyle from '../styles/Blog.module.css';
import Link from 'next/link';
import Image from 'next/image';
import noimage from "../public/Assets/noimage.png";

const BlogCard2 = ({ post, month }) => {
  return (
    <Link href='/blog/[slug]' as={`/blog/${post.slug}`} passHref >
      <div className={BlogStyle.Card}>
        <div className={BlogStyle.DisplayImage}>
          <Image
            src={post.feature_image === null ? noimage : post.feature_image}
            width={100}
            height={70}
            layout='responsive'
            alt=''
            loading='lazy'
          />
        </div>
        <Link href='/blog/tag/[tags]' as={`/blog/tag/${post.primary_tag?.slug}`}>
          <span className={BlogStyle.Tag}>
            {post.primary_tag === null ? null : post.primary_tag.name}
          </span>
        </Link>
        <h2>{post.title}</h2>
        <p>{post.excerpt}</p>
        <div className={BlogStyle.PublishInfo}>
          <span>
            {month[new Date(post.updated_at).getMonth()]}{' '}
            {new Date(post.updated_at).getDate()},{' '}
            {new Date(post.updated_at).getFullYear()}
          </span>
          <span>{post.reading_time} min read</span>
          <span style={{
            marginLeft: "20px"
          }} className={BlogStyle.author}>{post.primary_author.name}</span>
        </div>
        {/**
                     * <div className={BlogStyle.ProfileInfo}>
                    <img src="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bWVufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60" alt="" />
                    <h6>Raj Sharma</h6>
                    <span>Head Marketing @ Creatosaurus</span>
                </div>
                     */}
      </div>
    </Link>
  );
};

export default BlogCard2;
