import React, { useState } from 'react'
import BlogStyle from '../styles/Blog.module.css'
import { toast } from 'react-toastify'
import axios from 'axios'

const NewsLetterCard = () => {
    const [email, setemail] = useState("")
    const [loading, setLoading] = useState(false)

    const addUserToNewsLetter = async () => {
        try {
            const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (regex.test(email) === false) return toast.error("The email you've provided is invalid. Please enter a valid email address.")

            setLoading(true)
            await axios.post("https://api.app.creatosaurus.io/creatosaurus/newsletter/subscribe", {
                email,
                isSubscribed: true
            })

            toast.success("Congratulations! You have successfully subscribed to our newsletter.")
            setLoading(false)
        } catch (error) {
            toast.error("Newsletter subscription failed. Please retry or check internet and form accuracy.")
            setLoading(false)
        }
    }

    return (
        <div className={BlogStyle.NewsLetter}>
            <h2>Newsletter</h2>
            <div className={BlogStyle.InputWrapper}>
                <input type="email" placeholder="Enter Your Email" value={email} onChange={(e) => setemail(e.target.value)} />
                <button onClick={addUserToNewsLetter} style={{ display: 'flex', justifyContent: "center", alignItems: 'center', cursor:'pointer' }}>
                    {
                        loading ?
                            <span className='loader' /> :
                            "Subscribe"
                    }
                </button>
            </div>
            <h3>Connect with us</h3>

            <div className={BlogStyle.SocialMediaContainer}>
                <button onClick={() => {
                    window.open('https://twitter.com/creatosaurushq')
                }}>
                    <div className={BlogStyle.social}>
                        <svg style={{ marginRight: 10 }} width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg" >
                            <path d="M17.1789 7.84791C17.1903 8.00103 17.1903 8.15328 17.1903 8.30553C17.1903 12.9649 13.6439 18.3339 7.16275 18.3339C5.166 18.3339 3.311 17.7555 1.75 16.751C2.0335 16.7834 2.3065 16.7948 2.60138 16.7948C4.249 16.7948 5.76538 16.2383 6.97725 15.2889C5.42762 15.2565 4.12912 14.2415 3.68112 12.845C3.899 12.8774 4.11775 12.8993 4.347 12.8993C4.66288 12.8993 4.9805 12.8555 5.27538 12.7794C3.65925 12.4522 2.44913 11.0338 2.44913 9.32053V9.27678C2.919 9.53841 3.46412 9.70203 4.04163 9.72391C3.09225 9.09216 2.47013 8.01153 2.47013 6.78916C2.47013 6.13466 2.64425 5.53441 2.94963 5.01116C4.68475 7.14878 7.29313 8.54616 10.2174 8.69928C10.1631 8.43678 10.1299 8.16466 10.1299 7.89166C10.1299 5.94916 11.7014 4.36716 13.6544 4.36716C14.6694 4.36716 15.5855 4.79241 16.2295 5.48016C17.0258 5.32703 17.7888 5.03216 18.466 4.62878C18.2044 5.44691 17.647 6.13466 16.9164 6.57128C17.626 6.49428 18.3138 6.29828 18.9455 6.02528C18.466 6.72353 17.8666 7.34566 17.1789 7.84791Z" fill="black" />
                        </svg>
                        Twitter
                    </div>
                    <div className={BlogStyle.arrowDiv}>
                        <svg width="15" height="15" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M21.8333 11L11.8333 21M1 11L21.8333 11L1 11ZM21.8333 11L11.8333 1L21.8333 11Z" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </div>
                </button>

                <button onClick={() => {
                    window.open('https://www.youtube.com/watch?v=eS5tpAUEuzA')
                }}>
                    <div className={BlogStyle.social}>
                        <svg style={{ marginRight: 10 }} width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg" on>
                            <path d="M18.8934 6.65273C18.6922 5.90198 18.1016 5.3096 17.3517 5.10748C15.9814 4.73123 10.4996 4.7251 10.4996 4.7251C10.4996 4.7251 5.01856 4.71898 3.64743 5.0786C2.91243 5.27898 2.30518 5.88448 2.10218 6.63435C1.74081 8.0046 1.73731 10.8466 1.73731 10.8466C1.73731 10.8466 1.73381 13.7026 2.09256 15.0589C2.29381 15.8087 2.88443 16.4011 3.63518 16.6032C5.01943 16.9795 10.4864 16.9856 10.4864 16.9856C10.4864 16.9856 15.9683 16.9917 17.3386 16.633C18.0876 16.4317 18.6808 15.8402 18.8847 15.0904C19.2469 13.721 19.2496 10.8799 19.2496 10.8799C19.2496 10.8799 19.2671 8.02298 18.8934 6.65273ZM8.74606 13.4795L8.75043 8.22948L13.3066 10.8589L8.74606 13.4795Z" fill="black" />
                        </svg>
                        YouTube
                    </div>
                    <div className={BlogStyle.arrowDiv}>
                        <svg width="15" height="15" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M21.8333 11L11.8333 21M1 11L21.8333 11L1 11ZM21.8333 11L11.8333 1L21.8333 11Z" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </div>
                </button>

                <button onClick={() => {
                    window.open('https://www.instagram.com/creatosaurus')
                }}>
                    <div className={BlogStyle.social}>
                        <svg style={{ marginRight: 10 }} width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18.3283 8.11727C18.3187 7.4549 18.1962 6.79777 17.9617 6.17827C17.5513 5.1204 16.7157 4.2839 15.6578 3.8744C15.0462 3.64427 14.3996 3.5209 13.7451 3.5069C12.9033 3.46927 12.6364 3.45877 10.4997 3.45877C8.36295 3.45877 8.08907 3.45877 7.25345 3.5069C6.59982 3.5209 5.9532 3.64427 5.34157 3.8744C4.2837 4.2839 3.4472 5.1204 3.0377 6.17827C2.80757 6.7899 2.68332 7.43652 2.67107 8.09015C2.63345 8.93277 2.62207 9.19965 2.62207 11.3364C2.62207 13.4731 2.62207 13.7461 2.67107 14.5826C2.6842 15.2371 2.80757 15.8829 3.0377 16.4963C3.44807 17.5533 4.2837 18.3898 5.34245 18.7993C5.95145 19.0373 6.59807 19.172 7.25432 19.193C8.09695 19.2306 8.36382 19.242 10.5006 19.242C12.6373 19.242 12.9112 19.242 13.7468 19.193C14.4004 19.1799 15.0471 19.0565 15.6596 18.8264C16.7174 18.416 17.5531 17.5795 17.9634 16.5225C18.1936 15.91 18.3169 15.2643 18.3301 14.6089C18.3677 13.7671 18.3791 13.5003 18.3791 11.3626C18.3773 9.2259 18.3773 8.95465 18.3283 8.11727ZM10.4944 15.3771C8.2597 15.3771 6.44932 13.5668 6.44932 11.332C6.44932 9.09727 8.2597 7.2869 10.4944 7.2869C12.7274 7.2869 14.5396 9.09727 14.5396 11.332C14.5396 13.5668 12.7274 15.3771 10.4944 15.3771ZM14.7006 8.08052C14.1782 8.08052 13.7573 7.65877 13.7573 7.13727C13.7573 6.61577 14.1782 6.19402 14.7006 6.19402C15.2212 6.19402 15.6429 6.61577 15.6429 7.13727C15.6429 7.65877 15.2212 8.08052 14.7006 8.08052Z" fill="black" />
                            <path d="M10.4948 13.9596C11.946 13.9596 13.1224 12.7831 13.1224 11.3319C13.1224 9.88074 11.946 8.70432 10.4948 8.70432C9.04362 8.70432 7.86719 9.88074 7.86719 11.3319C7.86719 12.7831 9.04362 13.9596 10.4948 13.9596Z" fill="black" />
                        </svg>
                        Instagram
                    </div>

                    <div className={BlogStyle.arrowDiv}>
                        <svg width="15" height="15" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M21.8333 11L11.8333 21M1 11L21.8333 11L1 11ZM21.8333 11L11.8333 1L21.8333 11Z" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </div>

                </button>

                <button onClick={() => {
                    window.open('https://www.linkedin.com/company/creatosaurushq')
                }}>
                    <div className={BlogStyle.social}>
                        <svg style={{ marginRight: 10 }} width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.94506 6.04813C4.90171 6.04813 5.67722 5.27262 5.67722 4.31597C5.67722 3.35932 4.90171 2.5838 3.94506 2.5838C2.98841 2.5838 2.21289 3.35932 2.21289 4.31597C2.21289 5.27262 2.98841 6.04813 3.94506 6.04813Z" fill="black" />
                            <path d="M7.31261 7.36039V16.9704H10.2964V12.2181C10.2964 10.9641 10.5323 9.74964 12.0872 9.74964C13.6206 9.74964 13.6396 11.1834 13.6396 12.2972V16.9712H16.625V11.7011C16.625 9.11235 16.0677 7.12289 13.0419 7.12289C11.5892 7.12289 10.6154 7.9201 10.2172 8.67456H10.1769V7.36039H7.31261ZM2.4502 7.36039H5.43874V16.9704H2.4502V7.36039Z" fill="black" />
                        </svg>
                        LinkedIn
                    </div>

                    <div className={BlogStyle.arrowDiv}>
                        <svg width="15" height="15" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M21.8333 11L11.8333 21M1 11L21.8333 11L1 11ZM21.8333 11L11.8333 1L21.8333 11Z" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </div>

                </button>

                <button onClick={() => {
                    window.open('https://www.facebook.com/creatosaurushq')
                }}>
                    <div className={BlogStyle.social}>
                        <svg style={{ marginRight: 10 }} width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.7219 19.2228V12.0513H14.1413L14.5009 9.24347H11.7219V7.45497C11.7219 6.64472 11.9477 6.08997 13.1105 6.08997H14.584V3.5866C13.8674 3.5096 13.1464 3.47285 12.4254 3.47547C10.2869 3.47547 8.81866 4.78097 8.81866 7.1776V9.23822H6.41504V12.0461H8.82391V19.2228H11.7219Z" fill="black" />
                        </svg>
                        Facebook
                    </div>
                    <div className={BlogStyle.arrowDiv}>
                        <svg width="15" height="15" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M21.8333 11L11.8333 21M1 11L21.8333 11L1 11ZM21.8333 11L11.8333 1L21.8333 11Z" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </div>

                </button>
            </div>
        </div>
    )
}

export default NewsLetterCard