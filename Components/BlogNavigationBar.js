import React, { useState } from 'react';
import style from '../styles/BlogNavigationBar.module.css';
import Logo from '../public/Assets/Logo.svg';
import Link from 'next/link';
import Image from 'next/image';
import NavigationAuth from "../Components/NavigationAuth/NavigationAuth"

const BlogNavigationBar = (props) => {
    const [showLogin, setshowLogin] = useState(false)

    return (
        <React.Fragment>
            {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
            <div className={style.navigationBar}>
                <div className={style.row}>
                    <Link href="/">
                        <div className={style.logo}>
                            <Image src={Logo} alt='' />
                        </div>
                    </Link>

                    <Link href="/" passhref>
                        <span className={style.big}> CREATOSAURUS <span className={style.small}>| Blog</span> </span>
                    </Link>
                </div>

                <div className={style.searchContainer}>
                    <div className={style.loginButtonContainer}>
                        <Link href="/">
                            <span style={{
                                cursor: "pointer",
                                marginRight: "10px"
                            }}>
                                Home
                            </span>
                        </Link>
                        {props.inSideBlog ? null : <input type="search" onChange={(e) => {
                            props.setSearch(e.target.value)
                            props.handleOnChange(e.target.value)
                        }} placeholder="Search" />}
                        <a className={style.bold} href="https://calendly.com/malavwarke/creatosaurus" passHref>Book Demo</a>
                        <button className={style.loginButton} onClick={() => setshowLogin(true)}>Log in</button>
                        <button className={style.signupButton} onClick={() => setshowLogin(true)}>Sign up</button>
                    </div>
                </div>

            </div>
        </React.Fragment>
    )
}

export default BlogNavigationBar