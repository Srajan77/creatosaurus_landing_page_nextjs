import React, { useState, useEffect } from 'react'
import styles from './LoginEmailPopup.module.css'
import axios from 'axios'
import { useRouter } from 'next/router'
import constant from '../../constant'
import { toast } from 'react-toastify'

const LoginEmailPopup = ({ closeEmailLogin, addDataToLocalStorage, navAuth }) => {
    const [email, setEmail] = useState("")
    const [code, setCode] = useState("")
    const [loading, setLoading] = useState(false)
    const [resendCodeLoading, setresendCodeLoading] = useState(false)
    const [buttonActive, setButtonActive] = useState(true)

    const [emailPopup, setEmailPopup] = useState(true)
    const [isNewUser, setIsNewUser] = useState(null)
    const [timer, setTimer] = useState(59)
    const [intervalId, setIntervalId] = useState("")

    const router = useRouter()
    const { query } = router

    useEffect(() => {
        if (query.source === "appsumo") {
            checkEmail("appsumo")
        }
    }, [])


    const startInterval = () => {
        const id = setInterval(() => {
            setTimer((prev) => prev - 1)
        }, 1000);
        setIntervalId(id)
    }

    if (timer <= 1) {
        clearInterval(intervalId)
    }

    const checkEmail = (e) => {
        let value
        if (query.source === "appsumo") {
            value = query.email
            setEmail(value)
        } else {
            value = e.target.value
            setEmail(value)
        }

        if (new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/).test(value)) {
            setButtonActive(true)
        } else {
            setButtonActive(true)
        }
    }

    const sendEmailToUser = async () => {
        try {
            setLoading(true)
            const res = await axios.post(`${constant.url}send/user/email`, {
                email: email,
                timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
            })
            setLoading(false)
            setEmailPopup(false)
            setIsNewUser(res.data.isUserNew)
            startInterval()
        } catch (error) {
            setLoading(false)
        }
    }

    const addCode = (e) => {
        setCode(e.target.value)
    }

    const goBack = () => {
        if (emailPopup) {
            closeEmailLogin()
        } else {
            setEmailPopup(true)
            setTimer(59)
            setCode("")
            if (timer > 1) return clearInterval()
        }
    }

    const verifyCode = async () => {
        try {
            setLoading(true)
            const res = await axios.post(constant.url + "verify/user/code", {
                email: email,
                code: code,
            })
            setLoading(false)
            addDataToLocalStorage(res)
        } catch (error) {
            setLoading(false)
        }
    }

    const sendCode = async () => {
        try {
            setresendCodeLoading(true)
            await axios.post(constant.url + "resend/verify/email", {
                email: email
            })
            setresendCodeLoading(false)
            setTimer(59)
            setTimeout(() => {
                startInterval()
            }, 1000);
            toast.success("A new verification code has been sent to your email address")
        } catch (error) {
            setresendCodeLoading(false)
        }
    }

    return (
        <div className={navAuth ? `${styles.loginEmailPopupContainer} ${styles.loginEmailPopup} ${styles.hide}` : styles.loginEmailPopupContainer}>
            <div className={styles.card}>
                <div className={styles.head}>
                    <svg onClick={goBack} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.5 12H6M6 12L12 6M6 12L12 18" stroke="black" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                    <h2>{emailPopup ? "Continue with email" : isNewUser ? "You're almost signed up" : "Finish logging in"}</h2>
                </div>

                <p>
                    {
                        emailPopup ?
                            "We'll check if you have an account, and help create one if you don't."
                            :
                            isNewUser ?
                                `We have sent a code to ${email}, enter that and you will be finish signing up`
                                :
                                `We have sent a code to ${email}, enter that and you will be logged in.`
                    }
                </p>

                <div className={styles.inputContainer}>
                    <label>{emailPopup ? "Enter Email (personal or work)" : "Enter Code"}</label>
                    <input
                        value={emailPopup ? email : code}
                        type="text"
                        placeholder={emailPopup ? "david@example.com" : "code"}
                        onChange={emailPopup ? checkEmail : addCode} />
                </div>

                <button
                    onClick={buttonActive ? emailPopup ? sendEmailToUser : verifyCode : null}
                    className={buttonActive ? styles.active : null}>
                    {loading ? <span className={styles.loader} /> : emailPopup ? "Continue" : isNewUser ? "Sign up" : "Log in"}
                </button>


                {
                    emailPopup ? null :
                        <div className={styles.code}>
                            {
                                timer === 1 ?
                                    <>
                                        {
                                            resendCodeLoading ? <div>
                                                <span className={styles.loader} />
                                            </div> : <div>Didn&apos;t get the code? <span onClick={sendCode} className={styles.link}>Resend code</span></div>
                                        }
                                    </>
                                    :
                                    `Didn't get the code? Resend in ${timer} seconds`
                            }
                        </div>
                }
            </div>
        </div>
    )
}

export default LoginEmailPopup