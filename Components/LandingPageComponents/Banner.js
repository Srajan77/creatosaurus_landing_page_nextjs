import React, { useState, useEffect } from 'react';
import styles from '../LandingPageComponentCss/Banner.module.css';
import Image from 'next/image';
import BannerImage from '../../public/Assets/bannerImage.webp';
import Play from '../../public/Assets/Play.svg';
import People1 from '../../public/Assets/BannerPeople1.webp';
import People2 from '../../public/Assets/BannerPeople2.webp';
import People3 from '../../public/Assets/BannerPeople3.webp';
import People4 from '../../public/Assets/BannerPeople4.webp';
import { useGoogleOneTapLogin } from '@react-oauth/google';
import { useRouter } from 'next/router'
import axios from 'axios';
import NavigationAuth from '../NavigationAuth/NavigationAuth';

const Banner = () => {
  const router = useRouter()
  const { query } = router

  const [invitationCode, setinvitationCode] = useState(null)
  const [activeButton, setactiveButton] = useState(1);
  const [showVideo, setshowVideo] = useState(false);
  const [showLogin, setshowLogin] = useState(false)
  const [redirectTo, setredirectTo] = useState(null);

  useEffect(() => {
    setredirectTo(query.app)
    setinvitationCode(query.invitationCode)
  }, [query]);

  function setCookie(name, value) {
    if (process.env.NODE_ENV === 'development') {
      document.cookie = `${name}=${value}`
    } else {
      document.cookie = `${name}=${value}; domain=.creatosaurus.io`
    }
  }

  const addDataToLocalStorage = (res) => {
    try {
      setCookie("Xh7ERL0G", res.data.token)

      const queryString = router.asPath.split("?")

      if (redirectTo === null) {
        window.open("https://www.app.creatosaurus.io/", "_self");
      } else if (redirectTo === "muse") {
        const category = query.category;
        if (category !== undefined) return window.open(`https://www.muse.creatosaurus.io/editor?${queryString[1]}`, "_self");
        window.open("https://www.muse.creatosaurus.io", "_self");
      } else if (redirectTo === "apollo") {
        window.open("https://www.apollo.creatosaurus.io", "_self");
      } else if (redirectTo === "cache") {
        window.open("https://www.cache.creatosaurus.io", "_self");
      } else if (redirectTo === "captionator") {
        window.open("https://www.captionator.creatosaurus.io/", "_self");
        const template = query.template
        if (template !== undefined) return window.open(`https://www.captionator.creatosaurus.io?${queryString[1]}`, "_self");
      } else if (redirectTo === "quotes") {
        window.open("https://www.quotes.creatosaurus.io/", "_self");
      } else if (redirectTo === "hashtags") {
        window.open("https://www.hashtags.creatosaurus.io", "_self");
      } else if (redirectTo === "steno") {
        window.open("https://www.steno.creatosaurus.io/", "_self");
      } else {
        window.open("https://www.app.creatosaurus.io/", "_self");
      }
    } catch (error) {
      console.log(error)
    }
  }

  const login = async (response) => {
    try {
      const res = await axios.post("https://api.app.creatosaurus.io/creatosaurus/login", {
        token: response.credential,
        authType: "google",
        type: "oneTap",
        timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        invitationCodeFromUser: invitationCode,
      });

      addDataToLocalStorage(res)
    } catch (error) {
      console.log(error)
    }
  }

  useGoogleOneTapLogin({
    onSuccess: login,
    onError: () => console.log('Login Failed'),
  });

  return (
    <div>
      {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
      <div className={styles.headerContainer}>
        <div className={styles.topContainer}>
          <h1 className={styles.heading}>The only social media marketing tool you&apos;ll ever need</h1>
          <h6>Creatosaurus lets you easily curate ideas, design graphics, write AI content, edit videos, schedule post, search hashtags, craft articles, manage analytics, generate reports, apps & more—in one place.</h6>
          <button onClick={() => setshowLogin(true)}>Get Started—It&apos;s Free</button>
          {/* <span className={styles.subHeading}>On a mission to help creators tell 1 billion stories</span> */}
        </div>

        <div className={styles.buttonContainer} style={{ display: 'none' }}>
          <button onClick={() => setactiveButton(1)} className={activeButton === 1 ? styles.active : null}>Design graphics</button>
          <button onClick={() => setactiveButton(2)} className={activeButton === 2 ? styles.active : null}>Schedule post</button>
          <button onClick={() => setactiveButton(3)} className={activeButton === 3 ? styles.active : null}>App store</button>
        </div>

        <div className={styles.imageWrapper}>
          {
            showVideo ? <div className={styles.wrapper}>
              <iframe src="https://www.youtube.com/embed/eS5tpAUEuzA?autoplay=1" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div> : <div className={styles.img} >
              <Image priority={true} src={BannerImage} alt="" objectFit='contain' />
              <div className={styles.youtube} onClick={() => setshowVideo(true)} >
                <Image src={Play} alt="" objectFit='contain' />
              </div>
            </div>
          }
          <div className={styles.img1}>
            <Image src={People1} alt='' objectFit='contain' />
          </div>
          <div className={styles.img2}>
            <Image src={People2} alt='' objectFit='contain' />
          </div>
          <div className={styles.img3}>
            <Image src={People3} alt='' objectFit='contain' />
          </div>
          <div className={styles.img4}>
            <Image src={People4} alt='' objectFit='contain' />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;