import React, { useState } from 'react';
import Logo from '../../public/Assets/Logo.svg';
import Twitter from '../../public/Assets/Twitter.svg';
import Instagram from '../../public/Assets/Instagram.svg';
import Facebook from '../../public/Assets/Facebook.svg';
import LinkedIn from '../../public/Assets/LinkedIn.svg';
import YouTube from '../../public/Assets/YouTube.svg';
import stylesFooter from '../LandingPageComponentCss/Footer.module.css';
import Image from 'next/image';
import Link from 'next/link';
import NavigationAuth from '../NavigationAuth/NavigationAuth';

const Footer = () => {
  const [showLogin, setshowLogin] = useState(false)

  return (
    <>
      {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
      <div className={stylesFooter.footer}>
        <div className={stylesFooter.grid}>
          <div className={stylesFooter.list}>
            <div className={stylesFooter.row}>
              <div className={stylesFooter.logo}>
                <Image src={Logo} alt="" />
              </div>
              <h5>Creatosaurus</h5>
            </div>
            <p>All in One Creative & Marketing Platform to tell stories at scale</p>
            <div className={stylesFooter.imagesContainer}>
              <div className={stylesFooter.img}>
                <Link href='https://twitter.com/creatosaurushq' passHref>
                  <a target="_blank" rel="noopener noreferrer">
                    <Image src={Twitter} alt='' />
                  </a>
                </Link>
              </div>

              <div className={stylesFooter.img}>
                <Link href='https://www.instagram.com/creatosaurus/' passHref>
                  <a target="_blank" rel="noopener noreferrer">
                    <Image src={Instagram} alt='' />
                  </a>
                </Link>
              </div>

              <div className={stylesFooter.img}>
                <Link href='https://www.facebook.com/creatosaurushq/' passHref>
                  <Image src={Facebook} alt='' />
                </Link>
              </div>

              <div className={stylesFooter.img}>
                <Link href='https://www.linkedin.com/company/creatosaurushq/' passHref>
                  <a target="_blank" rel="noopener noreferrer">
                    <Image src={LinkedIn} alt='' />
                  </a>
                </Link>
              </div>

              <div className={stylesFooter.img}>
                <Link href='https://www.youtube.com/channel/UCSUwLNb8lt8OwbnLIFlhriA' passHref>
                  <a target="_blank" rel="noopener noreferrer">
                    <Image src={YouTube} alt='' />
                  </a>
                </Link>
              </div>
            </div>
          </div>

          <div className={stylesFooter.list}>
            <h5>Company</h5>
            <Link href='/about' passHref>About </Link>
            <Link href='https://www.linkedin.com/company/creatosaurushq/' passHref>
              <a target="_blank" rel="noopener noreferrer">
                Careers
              </a>
            </Link>
            <Link href='/contact' passHref>Contact</Link>
          </div>

          <div className={stylesFooter.list}>
            <h5>Products</h5>
            <Link href='/features'>Features</Link>
            <Link href='/pricing'>Pricing</Link>
            <Link href='https://www.youtube.com/watch?v=eS5tpAUEuzA'>
              <a target="_blank" rel="noopener noreferrer">
                Video Tutorial
              </a>
            </Link>
            <Link href='https://creatosaurus.canny.io/feature-requests'>
              <a target="_blank" rel="noopener noreferrer">
                Request Feature
              </a>
            </Link>
            <Link href='https://creatosaurus.canny.io/bug-board'>
              <a target="_blank" rel="noopener noreferrer">
                Product Feedback
              </a>
            </Link>
          </div>

          <div className={stylesFooter.list}>
            <h5>Resources</h5>
            <Link href='/blog'>Blog</Link>
            <Link href='https://www.facebook.com/groups/creatosaurus/' passHref>
              <a target="_blank" rel="noopener noreferrer">
                Facebook Community
              </a>
            </Link>
            <a style={{ cursor: 'pointer' }} onClick={() => setshowLogin(true)}>Sign up</a>
            <a style={{ cursor: 'pointer' }} onClick={() => setshowLogin(true)}>Log in</a>
          </div>

          <div className={stylesFooter.list}>
            <h5>Support</h5>
            <Link href='https://creatosaurus.tawk.help/' passHref>
              <a target="_blank" rel="noopener noreferrer">
                Help Center
              </a>
            </Link>
            <Link href='https://wa.me/919665047289' passHref>
              <a target="_blank" rel="noopener noreferrer">
                WhatsApp Support
              </a>
            </Link>
            <Link href='https://twitter.com/intent/tweet?text=Hey%20Creatosaurus&hashtags=StoriesMatter&via=creatosaurushq%20%F0%9F%99%8C%F0%9F%8F%BB%F0%9F%A6%96' passHref>
              <a target="_blank" rel="noopener noreferrer">
                Tweet at us
              </a>
            </Link>
            <Link href='https://calendly.com/malavwarke/creatosaurus' passHref>
              <a target="_blank" rel="noopener noreferrer">
                Book Demo
              </a>
            </Link>
            <Link href='https://creatosaurus.instatus.com/' passHref>
              <a target="_blank" rel="noopener noreferrer">
                Status
              </a>
            </Link>
            <Link href='https://creatosaurus.canny.io/' passHref>
              <a target="_blank" rel="noopener noreferrer">
                Roadmap
              </a>
            </Link>
          </div>
        </div>

        <div className={stylesFooter.footerBottom}>
          <div className={stylesFooter.subPart}>
            <div>Made with ❤️ for Creators</div>
            <Link href='/terms' passHref>Terms of Service</Link>
            <Link href='/privacy' passHref>Privacy Policy</Link>
          </div>
          <span>© 2023 Creatosaurus. All rights reserved by Creatosaurus</span>
        </div>
      </div>
    </>
  );
};

export default Footer;