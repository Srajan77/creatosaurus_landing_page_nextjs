import React, { useState } from 'react';
import styles from '../LandingPageComponentCss/TransperencySection.module.css';
import Image from 'next/image';
import Collaborate from '../../public/Assets/collaborate in realt.webp';
import Invite from '../../public/Assets/invite users.webp';
import Integration from '../../public/Assets/deep integrations.webp';
import Tools from '../../public/Assets/connect your favourite tools.webp';
import NavigationAuth from '../NavigationAuth/NavigationAuth';

const TransperencySection = () => {
  const [showLogin, setshowLogin] = useState(false)

  return (
    <>
      {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
      <div className={styles.transperencySection}>
        <h1>Transparency with simplicity</h1>
        <h5>Everyone in the team gets a complete view thanks to one unified platform.</h5>
        <div className={styles.inputContainer}>
          <button onClick={()=> setshowLogin(true)}>Get Started for Free</button>
        </div>

        <div className={styles.grid}>

          <div className={styles.card}>
            <div className={styles.box}>
              <div className={styles.box2}>
                <Image src={Collaborate} alt="" layout='fill' />
              </div>
            </div>
            <h6>Collaborate in real time</h6>
            <p>Work together asynchronously and in real-time</p>
          </div>

          <div className={styles.card}>
            <div className={`${styles.box} ${styles.box3}`} >
              <div className={styles.box2}>
                <Image src={Invite} alt="" layout='fill' />
              </div>
            </div>
            <h6>Invite and share</h6>
            <p>Make things simpler for your team and save time</p>
          </div>

          <div className={styles.card}>
            <div className={`${styles.box} ${styles.box4}`} >
              <div className={styles.box2}>
                <Image src={Integration} alt="" layout='fill' />
              </div>
            </div>
            <h6>Deep Integrations</h6>
            <p>Stay on top of your work by staying connected across apps</p>
          </div>

          <div className={styles.card}>
            <div className={`${styles.box} ${styles.box5}`} >
              <div className={styles.box2}>
                <Image src={Tools} alt="" layout='fill' />
              </div>
            </div>
            <h6>Connect your favourite tools</h6>
            <p>Favourite apps are right inside your creator studio</p>
          </div>


        </div>
      </div>
    </>
  );
};

export default TransperencySection;