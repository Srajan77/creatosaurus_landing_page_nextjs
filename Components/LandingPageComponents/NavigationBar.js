import React, { useState } from 'react'
import styles from '../LandingPageComponentCss/NavigationBar.module.css';
import Menu from '../../public/Assets/menu.svg';
import Logo from '../../public/Assets/Logo.svg';
import Image from 'next/image';
import Link from 'next/link';
import Close from '../../public/Assets/close.svg';
import NavigationAuth from '../NavigationAuth/NavigationAuth';

const NavigationBar = () => {
  const [open, setopen] = useState(false)
  const [showLogin, setshowLogin] = useState(false)

  const toggle = () => {
    setopen(prev => !prev)
  }

  return (
    <React.Fragment>
      {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
      {/* Mobile Style */}
      <div className={styles.navbar}>
        <React.Fragment>
          {
            open ?
              // side nav bar
              <div className={styles.sideMenu}>
                <div className={styles.header}>
                  <Link href="/" passHref>
                    <span className={styles.big}> CREATOSAURUS <span className={styles.small}>| Stories Matter</span> </span>
                  </Link>

                  <div className={styles.menu} onClick={toggle}>
                    <Image src={Close} alt='' />
                  </div>
                </div>

                <div className={styles.list}>
                  <Link href="/apps" passHref>Apps</Link>
                  <Link href="/blog" passHref>Blog</Link>
                  <Link href="/pricing" passHref>Pricing</Link>
                  <Link href="/login" passHref>Login</Link>
                  <Link href="/signup" passHref>Signup</Link>
                  <Link href="https://calendly.com/malavwarke/creatosaurus" passHref>Book Demo</Link>
                </div>
              </div>
              :
              // navigation bar
              <React.Fragment>
                <div className={styles.row}>
                  <Link href="/" passHref>
                    <div className={styles.logo}>
                      <Image src={Logo} alt='' />
                    </div>
                  </Link>

                  <Link href="/" passHref>
                    <span className={styles.big}> CREATOSAURUS <span className={styles.small}>| Stories Matter</span> </span>
                  </Link>
                </div>

                <div className={styles.menu} onClick={toggle}>
                  <Image src={Menu} alt='' />
                </div>
              </React.Fragment>
          }
        </React.Fragment>
      </div>


      <div className={styles.navbarLaptop}>
        <div className={styles.row}>
          <Link href="/" passHref>
            <div className={styles.logo}>
              <Image src={Logo} alt='' />
            </div>
          </Link>

          <Link href="/" passHref>
            <span className={styles.big}> CREATOSAURUS <span className={styles.small}>| Stories Matter</span> </span>
          </Link>
        </div>

        <div className={styles.list}>
          <Link href="/apps" passHref>Apps</Link>
          <Link href="/blog" passHref>Blog</Link>
          <Link href="/pricing" passHref>Pricing</Link>
        </div>

        <div className={styles.loginButtonContainer}>
          <a className={styles.bold} href="https://calendly.com/malavwarke/creatosaurus" passHref>Book Demo</a>
          <button className={styles.loginButton} onClick={() => setshowLogin(true)}>Log in</button>
          <button className={styles.signupButton} onClick={() => setshowLogin(true)}>Sign up</button>
        </div>
      </div>
    </React.Fragment>
  )
}

export default NavigationBar