import React from 'react'
import styles from '../LandingPageComponentCss/SupportedBy.module.css'
import TenK from '../../public/Assets/10k.webp'
import Echai from '../../public/Assets/echai.webp'
import Gsf from '../../public/Assets/gsf.webp'
import IHub from '../../public/Assets/i2ihub.webp'
import Innocity from '../../public/Assets/innocity.webp'
import Image from 'next/image';

const SupportedBy = () => {
  return (
    <div className={styles.supportedByContainer}>
       <h3>Supported by the Best</h3>
       <div className={styles.imageContainer}>
           <div>
              <Image src={Gsf} alt='' objectFit='contain' />
           </div>
           
           <div>
              <Image src={TenK} alt='' objectFit='contain'  />
           </div>

           <div>
              <Image src={IHub} alt='' objectFit='contain'  />
           </div>

           <div>
              <Image src={Echai} alt='' objectFit='contain'  />
           </div>

           <div>
              <Image src={Innocity} alt='' objectFit='contain'  />
           </div>
       </div>
    </div>
  )
}

export default SupportedBy