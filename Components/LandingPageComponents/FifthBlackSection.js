import React, { useState } from 'react';
import stylesFifthBlack from '../LandingPageComponentCss/FifthBlackSection.module.css';
import NavigationAuth from '../NavigationAuth/NavigationAuth';

const FifthBlackSection = () => {
  const [showLogin, setshowLogin] = useState(false)
  return (
    <>
      {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
      <div className={stylesFifthBlack.fifthBlackSection}>
        <h1>
          You focus on telling stories,
          we do everything else.
        </h1>
        <a onClick={() => setshowLogin(true)}>
          <button style={{ cursor: 'pointer' }}>Signup for Free</button>
        </a>
      </div>
    </>
  );
};

export default FifthBlackSection;
