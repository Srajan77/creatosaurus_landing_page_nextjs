import React from "react";
import stylesSecondBlack from "../LandingPageComponentCss/SecondBlackSection.module.css";

const SecondBlackSection = () => {
  return (
    <div className={stylesSecondBlack.secondBlackSection}>
      <p>
        As makers, creators, movers and shakers,
        <br />
        we are in this together.
      </p>
      <p>
        We have been building <br />
        something new for creators like us.
      </p>
      <p>
        All in one creator workspace.
        <br />
        That&apos;s right, we are making it.
      </p>
      <p>
        While it&apos;s a crazy idea and
        <br />
        has never been tried before,
        <br />
        we are up for the challenge.
      </p>
    </div>
  );
};

export default SecondBlackSection;
