import React from 'react'
import styles from '../LandingPageComponentCss/FourthBlackSection.module.css';

const FourthBlackSection = () => {
  return (
    <div className={styles.fourthBlackSection}>
      <h1>Built for Creators of Tomorrow</h1>
      <h5>Ultimate platofrm built specifically for you, to focus on things that matter.</h5>

      <div className={styles.grid}>
      <div className={`${styles.box} ${styles.box1}`}>
              <div className={styles.box2}>
                  <span>😌</span>
                  <h4>Content</h4>
                  <p>Create content that impacts <br /> people lives and have fun while creating it</p>
              </div>
           </div>

           <div className={`${styles.box} ${styles.box3}`}>
              <div className={styles.box2}>
                  <span>😍</span>
                  <h4>Community</h4>
                  <p>Build a community of trust and <br /> faith to tell stories together</p>
              </div>
           </div>

           <div className={`${styles.box} ${styles.box4}`}>
              <div className={styles.box2}>
                  <span>😇</span>
                  <h4>Culture</h4>
                  <p>Create a culture that makes your <br /> brand stand out and reflect differently</p>
              </div>
           </div>

           <div className={`${styles.box} ${styles.box5}`}>
              <div className={styles.box2}>
                  <span>😎</span>
                  <h4>Commerce</h4>
                  <p>Bring your business to <br /> life by doing what you love</p>
              </div>
           </div>
      </div>

    </div>
  )
}

export default FourthBlackSection