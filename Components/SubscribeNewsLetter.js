import React, { useState } from 'react'
import style from '../styles/Subscribe.module.css'
import { toast } from 'react-toastify'
import axios from 'axios'

const SubscribeNewsLetter = () => {
    const [email, setemail] = useState("")
    const [loading, setLoading] = useState(false)

    const addUserToNewsLetter = async () => {
        try {
            const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (regex.test(email) === false) return toast.error("The email you've provided is invalid. Please enter a valid email address.")

            setLoading(true)
            await axios.post("https://api.app.creatosaurus.io/creatosaurus/newsletter/subscribe", {
                email,
                isSubscribed: true
            })

            toast.success("Congratulations! You have successfully subscribed to our newsletter.")
            setLoading(false)
        } catch (error) {
            toast.error("Newsletter subscription failed. Please retry or check internet and form accuracy.")
            setLoading(false)
        }
    }

    return (
        <div className={style.subscribeCard}>
            <h1>Subscribe</h1>
            <p>
                Join our weekly digest. You&apos;ll also receive some of our best posts today.
            </p>
            <input type="email" placeholder="Enter Your Email" value={email} onChange={(e) => setemail(e.target.value)} />
            <button onClick={addUserToNewsLetter} style={{ display: 'flex', justifyContent: "center", alignItems: 'center' }}>
                {
                    loading ?
                        <span className='loader' /> :
                        "Subscribe"
                }
            </button>
        </div>
    );
}

export default SubscribeNewsLetter;