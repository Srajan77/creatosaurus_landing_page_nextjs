import src from "../../public/Assets/image_muse.png";

export const AppBlocksDataMuse = [
  {
    title: "Title Lorem Ipsum",
    description:
      "Instantly create free graphics online using 1000s of free templates. Edit your design & download your graphics with our free graphic design tool.",
    imageSrc: src,
  },
  {
    title: "Title Lorem Ipsum",
    description:
      "Instantly create free graphics online using 1000s of free templates. Edit your design & download your graphics with our free graphic design tool.",
    imageSrc: src,
  },
  {
    title: "Title Lorem Ipsum",
    description:
      "Instantly create free graphics online using 1000s of free templates. Edit your design & download your graphics with our free graphic design tool.",
    imageSrc: src,
  },
  {
    title: "Title Lorem Ipsum",
    description:
      "Instantly create free graphics online using 1000s of free templates. Edit your design & download your graphics with our free graphic design tool.",
    imageSrc: src,
  },
];
