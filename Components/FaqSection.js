import React, { useState } from "react";
import stylesFaq from "../styles/FaqSection.module.css";

const FrequentlyAskedQuestions = ({ title, content, id }) => {
  // state for faq
  const [isActive, setIsActive] = useState(false);

  return (
    <div className={stylesFaq.accordionItem} key={id}>
      {isActive ? (
        <div
          className={stylesFaq.accordionTitle}
          onClick={() => setIsActive(!isActive)}
          style={{ background: "rgba(248, 248, 249, 0.5)" }}
        >
          <div>{title}</div>
          <div className="isActive" style={{marginLeft : "10px"}}> {isActive ? "-" : "+"}</div>
        </div>
      ) : (
        <div
          className={stylesFaq.accordionTitle}
          onClick={() => setIsActive(!isActive)}
        >
          <div>{title}</div>
          <div className="isActive" style={{marginLeft : "10px"}}> {isActive ? "-" : "+"}</div>
        </div>
      )}

      {isActive && (
        <div className={stylesFaq.accordionContent}>
          <p>{content}</p>
        </div>
      )}
    </div>
  );
};

export default FrequentlyAskedQuestions;
