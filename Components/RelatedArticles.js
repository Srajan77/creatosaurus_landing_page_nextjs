import React from 'react'
import style from '../styles/RelatedArticles.module.css'
import Image from 'next/image'
import Link from 'next/link';

const RelatedArticle = ({ post }) => {
    const month = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sept',
        'Oct',
        'Nov',
        'Dec',
    ];
    return (
        <Link href='/blog/[slug]' as={`/blog/${post.slug}`} passHref>
            <div className={style.relatedArticleCard}>
                <div className={style.blogImage}>
                    {post.feature_image !== null ? (
                        <Image
                            src={post.feature_image}
                            width={100}
                            height={70}
                            layout='responsive'
                            alt=''
                        />
                    ) : null}
                </div>
                <div className={style.blogInfo}>
                    <div className={style.title}>
                        <h1 className={style.title}>{post.title}</h1>
                    </div>
                    <div className={style.info}>
                        <span>
                            {month[new Date(post.updated_at).getMonth()]}{' '}
                            {new Date(post.updated_at).getDate()},{' '}
                            {new Date(post.updated_at).getFullYear()}
                        </span>
                        <span>{post.reading_time} min read</span>
                        <span>{post.primary_author.slug}</span>
                    </div>
                </div>
            </div>
        </Link>
    );
}

export default RelatedArticle;