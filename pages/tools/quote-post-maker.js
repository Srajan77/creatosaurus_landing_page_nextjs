import React from 'react'
import Head from 'next/head';
import Image from 'next/image';
import museStyle from '../../styles/muse.module.css';
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar';
import logo from '../../public/Assets/Logo.svg';
import imgSrc from '../../public/Assets/muse_1.png';
import { AppBlocksDataMuse } from '../../Components/AppBlocksData/AppBlocksDataMuse';
import MainImage from "../../public/Assets/Tools/Group 899.jpg";
import apps from "../../appsInfo";
import FrequentlyAskedQuestions from '../../Components/FaqSection';
import FifthBlackSection from '../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../Components/LandingPageComponents/Footer';


const QuotePostMaker = () => {

    const svg = {
        leftArrow: (
            <svg
                width='14'
                height='10'
                viewBox='0 0 14 10'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
            >
                <path
                    d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
                    stroke='black'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                />
            </svg>
        ),
    };


    let data = [
        {
            title: "1000+ High-Quality Quote Post Templates",
            p: "Motivation, wisdom, nostalgia, gratitude — quotes evoke a sea of emotions to uplift spirits and enhance lives. Muse Quote Creator is home to an impressive collection of 1000+ quote poster templates  — choose the one that superbly captures the message you want to share with people."
        }, {
            title: "Quote Post Maker For Multiple Use Cases",
            p: "While quotes are a great way to share personal thoughts, they blend into any content marketing strategy. Whether for a blog post, email newsletter, website embed copy, or a social media post— make your brand shine through in your custom quote poster with Muse Quote Post Maker."
        } , {
            title: "Customizable Online Quote Poster Maker Templates",
            p: "Muse Quote Creator brings professional quote post templates to your fingertips — this is just for starters. Breathe life into your favourite quote poster template — drag and drop text, icons, images, elements, colours, and graphic components."
        }  , {
            title: "Edit-Friendly Quote Maker Templates",
            p: "With Muse Quote Creator, give your quote poster the perfect finish by playing around with its alignment, colour shades, layout dimensions, typography, and positioning. Dabble with a broad range of photo effects to give an aesthetic and nuanced touch to your quote post template in no time."
        } , {
            title: "Vast Library Of Stock Images For Your Quote Post",
            p: "A picture speaks a thousand words. Pair an insightful quote with a resonating image — your quote poster template will echo a million words. Muse Quote Maker has plenty of royalty-free and high-resolution stock photos — an easy search option helps you navigate the right one for your quote post in seconds."
        } , {
            title: "Save Your Custom Quote Poster In One Click",
            p: "You have mixed and matched text, colour, images, icons, and design elements on a quote post template. Voila! Your custom quote poster looks dashing and deserves a place to call home — save your Muse quote post creations for repurposing and zero-fuss, 24/7 access in your dashboard."
        } , {
            title: "Quote Post Maker For All Occasions",
            p: "Whether it is love, friendship, well-being, success, adversity, fitness, marriage, parenthood, or festivities — quotes are testaments to the human milestones we have crossed or are yet to venture. Muse Quote Maker has 1000+ quote post templates that dive into all those priceless moments and experiences."
        }  , {
            title: "Share Your Tailor-Made Quote Poster On Any Platform",
            p: "Have you unleashed your inner creative to the last drip in your online quote poster maker template? Don’t dilly dally to show your creation to the world! Instagram, Facebook, Twitter, Snapchat, Pinterest, or instant messaging — resize your quote post layout to tailor it for any platform and share in one click."
        } , {
            title: "Create Using Quote Post Templates Or From Square One",
            p: "Remove existing text and design elements to add the ones you prefer in readymade quote poster templates of varied layouts and themes. Another way is to pour out your creative brainchild in your inbuilt Muse Quote Creator artboard — from template creation to final-stage edits and personalization."
        } , {
            title: "Personalize Your Quote Maker Template",
            p: "With Muse Online Quote Poster Maker, give a personal dash to your quote poster in a few clicks. Is the perfect picture for your quote post waiting in your browser? All you got to do is upload it on your Muse artboard — drag and drop the image to any spot in your quote post template."
        }
    ]


    let faq = [
        {
            id: 1,
            title: "Is Muse Online Quote Poster Maker free to use?",
            content: "Access 1000+ post templates, stock images, & a wide range of graphic design tools with a free plan. Upgrade to paid plans for extra features.",
        }, {
            id: 2,
            title: "Who can use Muse Quote Maker?",
            content: "Muse Quote Maker is a free online design tool for marketers, graphic designers, content creators, and students.",
        }, {
            id: 3,
            title: "What can I do with Muse Instagram Quote Post Maker?",
            content: "Muse Quote Creator is a simple and fun free online graphic design tool to create personalized quote posts for your brand or personal sharing.",
        }, {
            id: 4,
            title: "Where can I put up quote posts I design using Muse Quote Creator?",
            content: "Embrace Muse Quote Maker in your marketing strategy — create custom quote posts for blogs, social media, website content, and more!",
        }, {
            id: 5,
            title: "Does Muse Online Quote Poster Maker let me save my quote posts?",
            content: "Save the customized quote post creations you design with Muse Quote Creator in one click — access the saved folder through your dashboard 24/7.",
        }
    ]

  return (
    <div>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>Free Online Quote Poster Maker & Creator | Muse</title>
                <meta
                    name="title"
                    content="Free Online Quote Poster Maker & Creator | Muse"
                />
                <meta
                    name="description"
                    content="Showcase your wisdom in a creative & visually appealing way — Muse Quote Creator offers thousands of customizable quote post templates. Try Quote Maker today."
                />
                <meta
                    property="og:title"
                    content="Free Online Quote Poster Maker & Creator | Muse"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Showcase your wisdom in a creative & visually appealing way — Muse Quote Creator offers thousands of customizable quote post templates. Try Quote Maker today."
                    key="description"
                />
            </Head>
            <NavigationBar />

            <div className={museStyle.top_block}>
                <div className={museStyle.dataBlocks_dataBox_reverse}>
                    <div className={museStyle.blockImage} style={{boxShadow:'none'}}>
                        <Image src={MainImage} alt='Failed to load image' />
                    </div>
                    <div className={museStyle.blockText}>
                        <div className={museStyle.blockText_icon}>
                            <Image src={logo} alt='Failed to load image' />
                        </div>
                        <div className={museStyle.blockText_title}>
                            <h1>Quote Creator - The Canvas For Your Inspirational Musings</h1>
                        </div>
                        <div className={museStyle.blockText_description}>
                            <p>
                            Pick any customizable template from Muse Quote Maker — splash in diverse text & visual elements to make your thoughts strike a chord with your viewers.
                            </p>
                        </div>
                        <div className={museStyle.blockText_button}>
                            <button onClick={() => {
                                openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&category=quotes&app=muse");
                            }}>Get Started For Free</button>
                        </div>
                    </div>
                </div>
            </div>



            <div className={museStyle.videoContainer}>
                <iframe
                    className={museStyle.videoBlock}
                    src='https://www.youtube.com/embed/TF3p_jpMQ1s'
                    title='YouTube video player'
                    allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                    allowFullScreen
                ></iframe>
            </div>



            <div className={museStyle.dataBlocks}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={AppBlocksDataMuse[0].imageSrc} alt='Failed to load image' />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h1>{res.title}</h1>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>

                                <div className={museStyle.blockText_button}>
                            <button onClick={() => {
                                openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&category=quotes&app=muse");
                            }}>Get Started For Free</button>
                        </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.redPoster}>
                <div className={museStyle.redPosterContainer}>
                    <div className={museStyle.redPosterTop}>
                        <div className={museStyle.redPosterHeading}>
                            <h1>Free online quote post maker and creator</h1>
                        </div>
                        <div className={museStyle.redPosterPara}>
                            <p>
                                Generate quote post for Instagram, Facebook at scale with our
                                massive 1 million library of quotes and easy to design beautiful
                                quote templates.
                            </p>
                        </div>
                        <div className={museStyle.redPosterButton}>
                            <button>Get Started for Free {svg.leftArrow}</button>
                        </div>
                    </div>
                    <div className={museStyle.redPosterBottom}>
                        <Image src={imgSrc} alt='muse_img loading...' />
                    </div>
                </div>
            </div>


            <div className={museStyle.how_to_container}>
                <h1>How To Use Muse Quote Creator?</h1>
                <div className={museStyle.card_container}>
                    <div className={museStyle.card}>
                        <h3>1. Choose a quote post template.</h3>
                    </div>
                    <div className={museStyle.card}>
                        <h3>2. Customize your quote poster template — drag and drop icons, images, elements, stickers, text, and fonts. If you want to remove any pre-existing component in your template, left-click on it and select delete. Use the upload option to incorporate personal images from your computer or browser into the quote post template.</h3>
                    </div>
                    <div className={museStyle.card}>
                        <h3>3. The edit toolbar is there to help you fine-tune your personalized quote post template by modifying its positioning, dimensions, colour hues, text, font, and alignment</h3>
                    </div>
                    <div className={museStyle.card}>
                        <h3>4. And that’s it! Save your quote post creation, which you can access in the saved folder section inside your Muse dashboard anytime.</h3>
                    </div>
                </div>
            </div>


            <div className={museStyle.discover_apps_container}>
                <div className={museStyle.more_apps_container}>
                    <h1>Discover more:</h1>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <button key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</button>;
                        })}
                    </div>
                </div>
            </div>


            <div className={museStyle.questionsContainer}>
                <div className={museStyle.heading}>
                    <span>Frequently Asked Questions</span>
                </div>
                {faq.map(({ title, content, id }) => (
                    <FrequentlyAskedQuestions title={title} content={content} key={id} />
                ))}
            </div>

            <FifthBlackSection />
            <Footer />
        </div>
  )
}

export default QuotePostMaker