import React from 'react'
import Head from 'next/head';
import Auth from '../Components/Auth/Auth'

const signup = () => {
  return (
    <div style={{ backgroundColor: '#f8f8f9', height: '100vh' }}>
      <Head>
        <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
        <title>Creatosaurus | Signup</title>
      </Head>
      <Auth title="Signup" />
    </div>
  )
}

export default signup