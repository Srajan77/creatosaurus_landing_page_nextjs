import React, { useState } from 'react'
import style from '../../styles/PricingPage.module.css';
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar';
import Footer from '../../Components/LandingPageComponents/Footer';
import Head from 'next/head';
import FifthBlackSection from '../../Components/LandingPageComponents/FifthBlackSection';
import axios from 'axios';
import NavigationAuth from '../../Components/NavigationAuth/NavigationAuth';
import ExitIntentPopup from '../../Components/ExitIntentPopup';

const Index = () => {
  const free = [
    'Unlimited Users',
    '1 workspace',
    '3 social media account',
    '10 scheduled post',
    'Free templates',
    'Free stock library',
    '2000 AI words',
    'Unlimited quotes',
    '5 Instagram hashtag search',
    'Online graphic design tool',
    '5 Transparent background',
    'Text editor',
  ]

  const creator = [
    '3 Users',
    '1 workspace',
    '5 social media account',
    '500 scheduled post',
    'Unlimited templates',
    'Unlimited stock library',
    '50000 AI words',
    'Unlimited quotes',
    '50 Instagram hashtag search',
    'Transperent background',
    'Multiple templete resolution',
    'Online graphic design tool',
    '50 Transparent background',
    'Text editor',
  ]

  const startup = [
    '15 Users maximum',
    '10 workspace',
    '10 social media account',
    '1000 scheduled post',
    'Unlimited templates',
    'Unlimited stock library',
    '75000 AI words /per user',
    'Unlimited quotes',
    '250 Instagram hashtag search',
    'Transperent background',
    'Multiple templete resolution',
    'Online graphic design tool',
    '200 Transparent background',
    'Text editor',
  ]

  const business = [
    'Unlimited users',
    'Unlimited workspace',
    'Unlimited social media account',
    'Unlimited scheduled post',
    'Unlimited templates',
    'Unlimited stock library',
    'Unlimited AI words',
    'Unlimited quotes',
    'Unlimited Instagram hashtag search',
    'Transperent background',
    'Multiple templete resolution',
    'Online graphic design tool',
    'Unlimited Transparent background',
    'Text editor',
  ]

  function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
  }


  const [loading, setLoading] = useState(null)
  const [error, setError] = useState(null)
  const [showLogin, setshowLogin] = useState(false)
  const [showExitIntent, setshowExitIntent] = useState(false)

  const upgradeUserPlan = async (planName) => {
    try {
      let token = getCookie('Xh7ERL0G');
      setLoading(planName)
      if (token === undefined || token === null) return setshowLogin(true)
      setError(null)
      const config = { headers: { "Authorization": `Bearer ${token}` } }

      await axios.post("https://api.app.creatosaurus.io/creatosaurus/userfeaturefactory/add/trial/plan", {
        planName: planName
      }, config)

      window.open('https://www.app.creatosaurus.io/', "_self")
      setLoading(null)
    } catch (error) {
      setLoading(false)
      setError(planName)
    }
  }

  const toggleExitIntent = (value) => {
    setshowExitIntent(value)
  }

  return (
    <React.Fragment>
      <Head>
        <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
      </Head>
      {/* <ExitIntentPopup toggleExitIntent={toggleExitIntent} showExitIntent={showExitIntent} /> */}
      <div>
        <NavigationBar />
        {showLogin ? <NavigationAuth close={() => setshowLogin(false)} planName={loading} /> : null}
        <div className={style.pricing}>
          <h1>Pricing</h1>
          <h5>One platform for your creative workflow. Simple plans for everyone. Get started for free. No credit card required.</h5>

          <div className={style.grid}>
            <div className={style.card}>
              <h2>Free</h2>
              <p>For individuals & small teams who want to tell stories.</p>
              <span className={style.price}><strong>$0</strong>/month</span>
              {
                free.map(data => {
                  return <div key={data} className={style.subCard}>
                    <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" clipRule="evenodd" d="M7.5 14C9.0913 14 10.6174 13.3679 11.7426 12.2426C12.8679 11.1174 13.5 9.5913 13.5 8C13.5 6.4087 12.8679 4.88258 11.7426 3.75736C10.6174 2.63214 9.0913 2 7.5 2C5.9087 2 4.38258 2.63214 3.25736 3.75736C2.13214 4.88258 1.5 6.4087 1.5 8C1.5 9.5913 2.13214 11.1174 3.25736 12.2426C4.38258 13.3679 5.9087 14 7.5 14V14ZM10.2802 7.03025C10.4169 6.8888 10.4925 6.69935 10.4908 6.5027C10.489 6.30605 10.4102 6.11794 10.2711 5.97889C10.1321 5.83983 9.94395 5.76095 9.7473 5.75924C9.55065 5.75754 9.3612 5.83313 9.21975 5.96975L6.75 8.4395L5.78025 7.46975C5.6388 7.33313 5.44935 7.25754 5.2527 7.25924C5.05605 7.26095 4.86794 7.33983 4.72889 7.47889C4.58983 7.61794 4.51095 7.80605 4.50924 8.0027C4.50754 8.19935 4.58313 8.3888 4.71975 8.53025L6.21975 10.0302C6.3604 10.1709 6.55113 10.2498 6.75 10.2498C6.94887 10.2498 7.1396 10.1709 7.28025 10.0302L10.2802 7.03025V7.03025Z" fill="black" />
                    </svg>
                    <span>{data}</span>
                  </div>
                })
              }
              <a target="_blank" href='https://www.app.creatosaurus.io/' rel="noreferrer">
                <button>Get started for free</button>
              </a>
            </div>

            <div className={style.card}>
              <h2>Creator</h2>
              <p>For individuals & small teams who want to tell stories.</p>
              <span className={style.price}><strong>$19</strong>/month</span>
              {
                creator.map(data => {
                  return <div key={data} className={style.subCard}>
                    <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" clipRule="evenodd" d="M7.5 14C9.0913 14 10.6174 13.3679 11.7426 12.2426C12.8679 11.1174 13.5 9.5913 13.5 8C13.5 6.4087 12.8679 4.88258 11.7426 3.75736C10.6174 2.63214 9.0913 2 7.5 2C5.9087 2 4.38258 2.63214 3.25736 3.75736C2.13214 4.88258 1.5 6.4087 1.5 8C1.5 9.5913 2.13214 11.1174 3.25736 12.2426C4.38258 13.3679 5.9087 14 7.5 14V14ZM10.2802 7.03025C10.4169 6.8888 10.4925 6.69935 10.4908 6.5027C10.489 6.30605 10.4102 6.11794 10.2711 5.97889C10.1321 5.83983 9.94395 5.76095 9.7473 5.75924C9.55065 5.75754 9.3612 5.83313 9.21975 5.96975L6.75 8.4395L5.78025 7.46975C5.6388 7.33313 5.44935 7.25754 5.2527 7.25924C5.05605 7.26095 4.86794 7.33983 4.72889 7.47889C4.58983 7.61794 4.51095 7.80605 4.50924 8.0027C4.50754 8.19935 4.58313 8.3888 4.71975 8.53025L6.21975 10.0302C6.3604 10.1709 6.55113 10.2498 6.75 10.2498C6.94887 10.2498 7.1396 10.1709 7.28025 10.0302L10.2802 7.03025V7.03025Z" fill="black" />
                    </svg>
                    <span>{data}</span>
                  </div>
                })
              }
              <a>
                <button style={loading === "creator" && showLogin === false ? { width: 215, display: 'flex', justifyContent: 'center', alignItems: 'center' } : null} onClick={() => upgradeUserPlan("creator")}>
                  {loading === "creator" && showLogin === false ? <span className='loader' /> : "Start a 14 days free trial"}
                </button>
              </a>
              <div style={{ display: 'flex', flexDirection: 'column', marginTop: 10 }}>
                <span style={{ fontSize: 8, color: 'red', fontWeight: 400 }}>{error === "creator" ? "Already taken a trial plan" : ""}</span>
                <span style={{ fontSize: 8, color: '#333333', fontWeight: 400 }}>No credit card required</span>
              </div>
            </div>


            <div className={style.card} style={{ borderColor: 'red' }}>
              <h2>Startup  <span style={{ fontSize: 12, color: '#333333', fontWeight: 400 }}>(Most Popular)</span></h2>
              <p>For individuals & small teams who want to tell stories.</p>
              <span className={style.price}><strong>$39</strong>/user/month</span>
              {
                startup.map(data => {
                  return <div key={data} className={style.subCard}>
                    <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" clipRule="evenodd" d="M7.5 14C9.0913 14 10.6174 13.3679 11.7426 12.2426C12.8679 11.1174 13.5 9.5913 13.5 8C13.5 6.4087 12.8679 4.88258 11.7426 3.75736C10.6174 2.63214 9.0913 2 7.5 2C5.9087 2 4.38258 2.63214 3.25736 3.75736C2.13214 4.88258 1.5 6.4087 1.5 8C1.5 9.5913 2.13214 11.1174 3.25736 12.2426C4.38258 13.3679 5.9087 14 7.5 14V14ZM10.2802 7.03025C10.4169 6.8888 10.4925 6.69935 10.4908 6.5027C10.489 6.30605 10.4102 6.11794 10.2711 5.97889C10.1321 5.83983 9.94395 5.76095 9.7473 5.75924C9.55065 5.75754 9.3612 5.83313 9.21975 5.96975L6.75 8.4395L5.78025 7.46975C5.6388 7.33313 5.44935 7.25754 5.2527 7.25924C5.05605 7.26095 4.86794 7.33983 4.72889 7.47889C4.58983 7.61794 4.51095 7.80605 4.50924 8.0027C4.50754 8.19935 4.58313 8.3888 4.71975 8.53025L6.21975 10.0302C6.3604 10.1709 6.55113 10.2498 6.75 10.2498C6.94887 10.2498 7.1396 10.1709 7.28025 10.0302L10.2802 7.03025V7.03025Z" fill="black" />
                    </svg>
                    <span>{data}</span>
                  </div>
                })
              }
              <a>
                <button style={loading === "startup" && showLogin === false ? { width: 215, display: 'flex', justifyContent: 'center', alignItems: 'center' } : null} onClick={() => upgradeUserPlan("startup")}>
                  {loading === "startup" && showLogin === false ? <span className='loader' /> : "Start a 14 days free trial"}
                </button>
              </a>
              <div style={{ display: 'flex', flexDirection: 'column', marginTop: 10 }}>
                <span style={{ fontSize: 8, color: 'red', fontWeight: 400 }}>{error === "startup" ? "Already taken a trial plan" : ""}</span>
                <span style={{ fontSize: 8, color: '#333333', fontWeight: 400 }}>No credit card required</span>
              </div>
            </div>


            {
              /**
               * <div className={style.card}>
              <h2>Business</h2>
              <p>For individuals & small teams who want to tell stories.</p>
              <span className={style.price}><p>Contact us for pricing</p></span>
              {
                business.map(data => {
                  return <div key={data} className={style.subCard}>
                    <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" clipRule="evenodd" d="M7.5 14C9.0913 14 10.6174 13.3679 11.7426 12.2426C12.8679 11.1174 13.5 9.5913 13.5 8C13.5 6.4087 12.8679 4.88258 11.7426 3.75736C10.6174 2.63214 9.0913 2 7.5 2C5.9087 2 4.38258 2.63214 3.25736 3.75736C2.13214 4.88258 1.5 6.4087 1.5 8C1.5 9.5913 2.13214 11.1174 3.25736 12.2426C4.38258 13.3679 5.9087 14 7.5 14V14ZM10.2802 7.03025C10.4169 6.8888 10.4925 6.69935 10.4908 6.5027C10.489 6.30605 10.4102 6.11794 10.2711 5.97889C10.1321 5.83983 9.94395 5.76095 9.7473 5.75924C9.55065 5.75754 9.3612 5.83313 9.21975 5.96975L6.75 8.4395L5.78025 7.46975C5.6388 7.33313 5.44935 7.25754 5.2527 7.25924C5.05605 7.26095 4.86794 7.33983 4.72889 7.47889C4.58983 7.61794 4.51095 7.80605 4.50924 8.0027C4.50754 8.19935 4.58313 8.3888 4.71975 8.53025L6.21975 10.0302C6.3604 10.1709 6.55113 10.2498 6.75 10.2498C6.94887 10.2498 7.1396 10.1709 7.28025 10.0302L10.2802 7.03025V7.03025Z" fill="black" />
                    </svg>
                    <span>{data}</span>
                  </div>
                })
              }
              <a target="_blank" href='https://www.app.creatosaurus.io/' rel="noreferrer">
                <button>Get started for Free</button>
              </a>
              <span style={{ fontSize: 8, color: '#333333', fontWeight: 400 }}>Free trial available, no credit card required</span>
          </div>
               */
            }
          </div>

          <div className={style.helpBox}>
            <div className={style.box}>
              <h2>Need Unlimited Plan?</h2>
              <a style={{ marginTop: 20 }} target="_blank" href='https://calendly.com/malavwarke/creatosaurus' rel="noreferrer">
                <button>Let&apos;s Talk</button>
              </a>
            </div>
          </div>
        </div>

        <FifthBlackSection />
        <Footer />
      </div>
    </React.Fragment>
  )
}

export default Index