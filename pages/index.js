import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import NavigationBar from '../Components/LandingPageComponents/NavigationBar';
import Banner from '../Components/LandingPageComponents/Banner';
import jwtDecode from 'jwt-decode';

const SupportedBy = dynamic(() => import("../Components/LandingPageComponents/SupportedBy"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const FirstBlackSection = dynamic(() => import("../Components/LandingPageComponents/FirstBlackSection"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const AppInfoSection = dynamic(() => import("../Components/LandingPageComponents/AppInfoSection"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const AppsCardBlock = dynamic(() => import("../Components/LandingPageComponents/AppsCardBlock"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const Features = dynamic(() => import("../Components/LandingPageComponents/Features"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const FifthBlackSection = dynamic(() => import("../Components/LandingPageComponents/FifthBlackSection"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});


const Footer = dynamic(() => import("../Components/LandingPageComponents/Footer"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const FourthBlackSection = dynamic(() => import("../Components/LandingPageComponents/FourthBlackSection"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const SecondBlackSection = dynamic(() => import("../Components/LandingPageComponents/SecondBlackSection"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const ThirdBlackSection = dynamic(() => import("../Components/LandingPageComponents/ThirdBlackSection"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const TransperencySection = dynamic(() => import("../Components/LandingPageComponents/TransperencySection"), {
  loading: () => <p>Loading ...</p>,
  ssr: false
});

const Index = () => {
  const [showComponent, setshowComponent] = useState(false)

  function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
  }

  useEffect(() => {
    const value = getCookie('Xh7ERL0G');
    if (value) {
      const decoded = jwtDecode(value);
      const expirationTime = (decoded.exp * 1000) - 60000
      if (Date.now() >= expirationTime) {
        // Do something if expired
      } else {
        return window.open(`https://www.app.creatosaurus.io`, "_self");
      }
    }

    setshowComponent(true)
  }, []);

  if (!showComponent) return <div id="loading-auth-box" />

  return (
    <div>
      <Head>
        <title>
          Creatosaurus - All in One Creative & Marketing Platform | Storytelling at Scale
        </title>
        <link rel="icon" href="/Assets/creatosaurusfavicon.ico" />
        <meta
          name="description"
          content="Creatosaurus helps marketing teams streamline their workflow and run a scalable marketing strategy—from your curation and collaboration to creation and distribution to analytics and apps."
        />
        <meta
          property="og:title"
          content="Creatosaurus - All in One Creative & Marketing Platform | Storytelling at Scale"
          key="title"
        />
        <meta
          property="og:description"
          content="Creatosaurus helps marketing teams streamline their workflow and run a scalable marketing strategy—from your curation and collaboration to creation and distribution to analytics and apps."
          key="description"
        />
        <meta
          property="og:description"
          content="Creatosaurus helps marketing teams streamline their workflow and run a scalable marketing strategy—from your curation and collaboration to creation and distribution to analytics and apps."
          key="description"
        />

        <meta property="og:site_name" content="Creatosaurus"></meta>

        <meta
          property="og:url"
          content="https://www.creatosaurus.io/"
          key="url"
        />
        <meta property="og:type" content="Website" key="type" />
        <meta
          property="og:image"
          content="/Assets/creatosaurusopengraphimage.webp"
        />

        {/* Twitter */}
        <meta
          name="twitter:title"
          content="Creatosaurus - All in One Creative & Marketing Platform | Storytelling at Scale"
        />
        <meta
          name="twitter:description"
          content="Creatosaurus helps marketing teams streamline their workflow and run a scalable marketing strategy—from your curation and collaboration to creation and distribution to analytics and apps."
        />
        {/* <meta
          name="twitter:image"
          content="/Assets/creatosaurusopengraphimage.webp"
          key="image"
        /> */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@creatosaurushq" />
        <meta name="twitter:url" content="https://www.creatosaurus.io/" />
      </Head>
      <div>
        <NavigationBar />
        <Banner />
        <SupportedBy />
        <br />
        <br />
        <br />
        <FirstBlackSection />
        <Features />
        <SecondBlackSection />
        <AppInfoSection />
        <ThirdBlackSection />
        <AppsCardBlock />
        <FourthBlackSection />
        <TransperencySection />
        <FifthBlackSection />
        <Footer />
      </div>
    </div>
  );
};

export default Index;