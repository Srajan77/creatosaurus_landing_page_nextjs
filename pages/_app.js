import '../styles/globals.css';
import NextNProgress from 'nextjs-progressbar';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import dynamic from 'next/dynamic'
import { GoogleOAuthProvider } from '@react-oauth/google';
import FacebookPixel from './scripts/FacebookPixel';
import Cookie from './Cookie';

const GoogleAnalytics = dynamic(() => import("./scripts/GoogleAnalytics"), {
  ssr: false
})

const MicrosoftClarity = dynamic(() => import("./scripts/MicrosoftClearity"), {
  ssr: false
})

const Tawkto = dynamic(() => import("./scripts/Tawkto"), {
  ssr: false
})

const Plausible = dynamic(() => import("./scripts/Plausible"), {
  ssr: false
})

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GoogleOAuthProvider clientId="349325227003-mg6k6mpcusljic2ukt1iukv1uepm6sr2.apps.googleusercontent.com">
        <ToastContainer
          position='top-right'
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <NextNProgress height={2} color='#ff4359' />

        {/* Google Analytics */}
        <GoogleAnalytics />

        {/* Microsoft Clarity */}
        <MicrosoftClarity />

        {/* Facebook Analytics */}
        <FacebookPixel />

        {/* Tawk.to */}
        <Tawkto />

        {/* Plausible */}
        <Plausible />

        <Cookie />

        <Component {...pageProps} />
      </GoogleOAuthProvider>
    </>
  );
}

export default MyApp;
