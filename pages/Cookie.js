import React, { useState, useEffect } from 'react'
import styles from '../styles/Cookie.module.css'

const Cookie = () => {

    const [showCookie, setshowCookie] = useState(false)

    useEffect(() => {
        let data = localStorage.getItem("cookie")
        if (data === null) setshowCookie(true)
    }, [])

    const addDataToLocalStorage = (value) => {
          localStorage.setItem("cookie", value)
          setshowCookie(false)
    }

    return (
        <div>
            {
                showCookie ? <div className={styles.cookieContainer}>
                    <p>Creatosaurus uses cookies to deliver a seamless & personalised experience. To know more check our <a href='https://www.creatosaurus.io/privacy' target="_blank" rel="noopener noreferrer">privacy policy</a>.</p>
                    <div className={styles.buttonContainer}>
                        <button onClick={() => addDataToLocalStorage("reject")}>Reject</button>
                        <button onClick={() => addDataToLocalStorage("accept")}>Accept</button>
                    </div>
                </div> : null
            }
        </div>
    )
}

export default Cookie