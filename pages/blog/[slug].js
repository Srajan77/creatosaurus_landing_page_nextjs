import Head from 'next/head';
import BlogNavigationBar from '../../Components/BlogNavigationBar';
import Footer from '../../Components/LandingPageComponents/Footer';
import { getSinglePost, getPostsByTags } from '../../lib/posts';
import stylesSlug from '../../styles/Slug.module.css';
import { useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import FifthBlackSection from '../../Components/LandingPageComponents/FifthBlackSection';
import ArticleByCard from '../../Components/ArticleByCard';
import RelatedArticle from '../../Components/RelatedArticles';
import axios from 'axios';
import { toast } from 'react-toastify'

export async function getServerSideProps(context) {
  const post = await getSinglePost(context.params.slug);
  const relatedArticles = await getPostsByTags(post?.primary_tag?.slug)

  if (!post) {
    return {
      notFound: true,
    };
  }

  return {
    props: { post, relatedArticles },
  };
}

const PostPage = ({ post, relatedArticles }) => {
  const [contents, setContents] = useState([]);
  const [contentsId, setContentsId] = useState([]);
  const [email, setemail] = useState("")
  const [loading, setLoading] = useState(false)
  const month = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];
  let slug = post.slug;

  let meta_title = post.meta_title;
  let meta_description = post.meta_description;
  let twitterTitle = post.twitter_title;
  let twitterImg = post.twitter_image;
  let twitterDis = post.twitter_description;
  let ogTitle = post.og_title;
  let ogDis = post.og_description;
  let ogImg = post.og_image;
  // author info
  let author_name = post.primary_author.name;
  let author_bio = post.primary_author.bio;
  let author_profile = post.primary_author.profile_image;
  let tags = '...';
  if (post.primary_tag != null) {
    tags = post.primary_tag.name;
  }
  // tags

  // adding style for scrolling
  useEffect(() => {
    let b = document.getElementsByTagName('h2');
    let a = [];
    let c = [];
    if (contents.length == 0) {
      for (let i = 0; i < b.length; i++) {
        a.push(b[i].innerText);
        c.push(b[i].id);
      }
      setContents(a);
      setContentsId(c);
    }

    const progressBarScroll = () => {
      let winScroll =
        document.body.scrollTop || document.documentElement.scrollTop,
        height =
          document.documentElement.scrollHeight -
          document.documentElement.clientHeight,
        scrolled = (winScroll / height) * 100;

      if (document.getElementById('progressBar')) {
        document.getElementById('progressBar').style.width = scrolled + '%';
      }
    };

    window.addEventListener('scroll', (e) => {
      progressBarScroll();
      let scrollTop = window.scrollY;
      if (scrollTop >= 1000) {
        if (document.getElementById('InBlogLeftSide')) {
          document.getElementById('InBlogLeftSide').style.opacity = 1;
          document.getElementById('InBlogRightSide').style.opacity = 1;
        }
      } else {
        if (document.getElementById('InBlogLeftSide')) {
          document.getElementById('InBlogLeftSide').style.opacity = 0;
          document.getElementById('InBlogRightSide').style.opacity = 0;
        }
      }
    });
  }, []); // eslint-disable-next-line react-hooks/exhaustive-deps

  const shareOnTwitter = (slug, meta_title) => {
    const params = 'width=500,height=500,top=200,left=700,_self';
    const tweetUrl = `https://twitter.com/intent/tweet?url=https://www.creatosaurus.io/blog/${slug}&text=${encodeURIComponent(meta_title)}&via=creatosaurushq`;
    window.open(tweetUrl, 'test', params);
  }

  const shareOnLinkedin = (slug) => {
    const params = 'width=500,height=500,top=200,left=700,_self';
    const postUrl = `https://www.linkedin.com/shareArticle?mini=true&url=https://www.creatosaurus.io/blog/${slug}`
    window.open(
      postUrl,
      'test',
      params
    )
  }

  const shareOnFacebook = (slug, meta_title) => {
    const params = 'width=500,height=500,top=200,left=700,_self';
    const postUrl = `https://www.facebook.com/sharer/sharer.php?u=https://www.creatosaurus.io/blog/${slug}&text=${encodeURIComponent(
      meta_title
    )}`
    window.open(
      postUrl,
      'test',
      params
    )
  }

  const addUserToNewsLetter = async () => {
    try {
      const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      if (regex.test(email) === false) return toast.error("The email you've provided is invalid. Please enter a valid email address.")

      setLoading(true)
      await axios.post("https://api.app.creatosaurus.io/creatosaurus/newsletter/subscribe", {
        email,
        isSubscribed: true
      })

      toast.success("Congratulations! You have successfully subscribed to our newsletter.")
      setLoading(false)
    } catch (error) {
      toast.error("Newsletter subscription failed. Please retry or check internet and form accuracy.")
      setLoading(false)
    }
  }


  return (
    <main>
      <div>
        <Head>
          <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
          <title>{meta_title}</title>
          <meta name='description' content={meta_description} />
          {/* default social meta tags  */}
          <meta name='og:title' content={ogTitle} />
          <meta name='og:image' content={ogImg} />
          <meta name='og:description' content={ogDis} />
          <meta
            name='og:url'
            content={`https://www.creatosaurus.io/blog/${slug}`}
          />
          <meta name='og:card' content='summary_large_image' />
          <meta property='og:type' content='Website' key='type' />
          <meta property='og:site_name' content='Creatosaurus Blog' />
          {/* twitter meta tags  */}
          <meta name='twitter:title' content={twitterTitle} key='title' />
          <meta name='twitter:image' content={twitterImg} key='image' />
          <meta property='twitter:site_name' content='Creatosaurus Blog' />
          <meta
            name='twitter:description'
            content={twitterDis}
            key='description'
          />
          <meta
            name='twitter:url'
            content={`https://www.creatosaurus.io/blog/${slug}`}
            key='url'
          />
          <meta name='twitter:card' content='summary_large_image' />
          <meta property='twitter:type' content='Website' key='type' />
          <script
            async=''
            src='https://platform.twitter.com/widgets.js'
            charset='utf-8'
          ></script>
        </Head>
        <BlogNavigationBar inSideBlog={true} />
        <div className={stylesSlug.articleContainer}>
          <div className={stylesSlug.topSection}>
            <h1>{post.title}</h1>
            <p>{post.meta_description}</p>
            <div>
              {/* all tags  */}
              <span className={stylesSlug.tag}>{tags}</span>
              <span>{author_name}</span> {/* author name */}
              <span>
                {month[new Date(post.updated_at).getMonth()]}{' '}
                {new Date(post.updated_at).getDate()},{' '}
                {new Date(post.updated_at).getFullYear()}
              </span>
              <span>{post.reading_time} min read</span>
            </div>
            <div className={stylesSlug.blogLeft}>
              <div className={stylesSlug.blogSideBox} id='InBlogLeftSide'>
                <div className={stylesSlug.sideChildBox}>
                  <h4>Contents</h4>
                  <div className={stylesSlug.allHeadings}>
                    {contents.map((res, index) => {
                      return (
                        <Link
                          href={`#${contentsId[index]}`}
                          scroll={true}
                          key={`${res}-${index}`}
                          passHref
                        >
                          <a>{res}</a>
                        </Link>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
            <div className={stylesSlug.blogRight}>
              <div className={stylesSlug.blogSideBox} id='InBlogRightSide'>
                <div className={stylesSlug.sideChildBox}>
                  {author_profile && (
                    <div className={stylesSlug.authorProfilePic}>
                      <Image src={author_profile} alt='Failed to load image' width={50} height={50} />
                    </div>
                  )}
                  {!author_profile && (
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      width='24'
                      height='24'
                      viewBox='0 0 24 24'
                    >
                      <path
                        fill='#200E32'
                        d='M5.84846399,13.5498221 C7.28813318,13.433801 8.73442297,13.433801 10.1740922,13.5498221 C10.9580697,13.5955225 11.7383286,13.6935941 12.5099314,13.8434164 C14.1796238,14.1814947 15.2696821,14.7330961 15.73685,15.6227758 C16.0877167,16.317132 16.0877167,17.1437221 15.73685,17.8380783 C15.2696821,18.727758 14.2228801,19.3149466 12.4926289,19.6174377 C11.7216312,19.7729078 10.9411975,19.873974 10.1567896,19.9199288 C9.43008411,20 8.70337858,20 7.96802179,20 L6.64437958,20 C6.36753937,19.9644128 6.09935043,19.9466192 5.83981274,19.9466192 C5.05537891,19.9062698 4.27476595,19.8081536 3.50397353,19.6530249 C1.83428106,19.3327402 0.744222763,18.7633452 0.277054922,17.8736655 C0.0967111971,17.5290284 0.00163408158,17.144037 0.000104217816,16.752669 C-0.00354430942,16.3589158 0.0886574605,15.9704652 0.268403665,15.6227758 C0.72692025,14.7330961 1.81697855,14.1548043 3.50397353,13.8434164 C4.27816255,13.6914539 5.06143714,13.5933665 5.84846399,13.5498221 Z M8.00262682,-1.16351373e-13 C10.9028467,-1.16351373e-13 13.2539394,2.41782168 13.2539394,5.40035587 C13.2539394,8.38289006 10.9028467,10.8007117 8.00262682,10.8007117 C5.10240696,10.8007117 2.75131423,8.38289006 2.75131423,5.40035587 C2.75131423,2.41782168 5.10240696,-1.16351373e-13 8.00262682,-1.16351373e-13 Z'
                        transform='translate(4 2)'
                      />
                    </svg>
                  )}
                  <h4>{author_name}</h4>
                  <p>{author_bio}</p>
                </div>
                <div className={stylesSlug.sideChildBox}>
                  <h4>Newsletter</h4>
                  <div className={stylesSlug.inputwrapper}>
                    <input type="email" value={email} placeholder='Enter Your Email' onChange={(e) => setemail(e.target.value)} />
                  </div>
                  <button onClick={addUserToNewsLetter} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    {
                      loading ? <span className='loader' /> : "Subscribe"
                    }
                  </button>
                </div>
                <div className={stylesSlug.sideChildBox}>
                  <h4>Share This Article</h4>
                  <div className={stylesSlug.socialMedia}>
                    <button
                      onClick={() => {
                        shareOnTwitter(slug, post.title)
                      }}
                      className={stylesSlug.twitter}
                    >
                      <svg
                        style={{ marginRight: 0 }}
                        width='21'
                        height='22'
                        viewBox='0 0 21 22'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          d='M17.1789 7.84791C17.1903 8.00103 17.1903 8.15328 17.1903 8.30553C17.1903 12.9649 13.6439 18.3339 7.16275 18.3339C5.166 18.3339 3.311 17.7555 1.75 16.751C2.0335 16.7834 2.3065 16.7948 2.60138 16.7948C4.249 16.7948 5.76538 16.2383 6.97725 15.2889C5.42762 15.2565 4.12912 14.2415 3.68112 12.845C3.899 12.8774 4.11775 12.8993 4.347 12.8993C4.66288 12.8993 4.9805 12.8555 5.27538 12.7794C3.65925 12.4522 2.44913 11.0338 2.44913 9.32053V9.27678C2.919 9.53841 3.46412 9.70203 4.04163 9.72391C3.09225 9.09216 2.47013 8.01153 2.47013 6.78916C2.47013 6.13466 2.64425 5.53441 2.94963 5.01116C4.68475 7.14878 7.29313 8.54616 10.2174 8.69928C10.1631 8.43678 10.1299 8.16466 10.1299 7.89166C10.1299 5.94916 11.7014 4.36716 13.6544 4.36716C14.6694 4.36716 15.5855 4.79241 16.2295 5.48016C17.0258 5.32703 17.7888 5.03216 18.466 4.62878C18.2044 5.44691 17.647 6.13466 16.9164 6.57128C17.626 6.49428 18.3138 6.29828 18.9455 6.02528C18.466 6.72353 17.8666 7.34566 17.1789 7.84791Z'
                          fill='black'
                        />
                      </svg>
                    </button>

                    <button
                      onClick={() =>
                        shareOnLinkedin(slug, post.title)
                      }
                      className={stylesSlug.linkedin}
                    >
                      <svg
                        style={{ marginRight: 0 }}
                        width='19'
                        height='20'
                        viewBox='0 0 19 20'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          d='M3.94506 6.04813C4.90171 6.04813 5.67722 5.27262 5.67722 4.31597C5.67722 3.35932 4.90171 2.5838 3.94506 2.5838C2.98841 2.5838 2.21289 3.35932 2.21289 4.31597C2.21289 5.27262 2.98841 6.04813 3.94506 6.04813Z'
                          fill='black'
                        />
                        <path
                          d='M7.31261 7.36039V16.9704H10.2964V12.2181C10.2964 10.9641 10.5323 9.74964 12.0872 9.74964C13.6206 9.74964 13.6396 11.1834 13.6396 12.2972V16.9712H16.625V11.7011C16.625 9.11235 16.0677 7.12289 13.0419 7.12289C11.5892 7.12289 10.6154 7.9201 10.2172 8.67456H10.1769V7.36039H7.31261ZM2.4502 7.36039H5.43874V16.9704H2.4502V7.36039Z'
                          fill='black'
                        />
                      </svg>
                    </button>

                    <button
                      onClick={() => {
                        shareOnFacebook(slug, post.title)
                      }}
                      className={stylesSlug.facebook}
                    >
                      <svg
                        style={{ marginRight: 0 }}
                        width='21'
                        height='22'
                        viewBox='0 0 21 22'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          d='M11.7219 19.2228V12.0513H14.1413L14.5009 9.24347H11.7219V7.45497C11.7219 6.64472 11.9477 6.08997 13.1105 6.08997H14.584V3.5866C13.8674 3.5096 13.1464 3.47285 12.4254 3.47547C10.2869 3.47547 8.81866 4.78097 8.81866 7.1776V9.23822H6.41504V12.0461H8.82391V19.2228H11.7219Z'
                          fill='black'
                        />
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={stylesSlug.headerImage}>
            <Image
              width={100}
              height={65}
              layout='responsive'
              src={post.feature_image}
              alt='blogpic'
            />
          </div>

          <div className={stylesSlug.body}>
            <div dangerouslySetInnerHTML={{ __html: post.html }} />
          </div>
          <ArticleByCard post={post} />
        </div>

        <div className={stylesSlug.relatedArticlesContainer}>
          <span>RELATED ARTICLES</span>
          <div className={stylesSlug.articles}>
            {relatedArticles.map((data, index) => {
              if (index > 3) return
              return <RelatedArticle key={index} post={data} />
            })}
          </div>
        </div>
        <FifthBlackSection />
        <Footer />
      </div>
    </main>
  );
};

export default PostPage;

