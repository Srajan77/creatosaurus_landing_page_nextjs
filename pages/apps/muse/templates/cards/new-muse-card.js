import React from "react";
import Head from "next/head";
import Image from "next/image";
import museStyle from "../../../../../styles/muse.module.css";
import NavigationBar from "../../../../../Components/LandingPageComponents/NavigationBar";
import imgSrc from "../../../../../public/Assets/muse_1.png";
import apps from "../../../../../appsInfo";
import FrequentlyAskedQuestions from "../../../../../Components/FaqSection";
import FifthBlackSection from "../../../../../Components/LandingPageComponents/FifthBlackSection";
import Footer from "../../../../../Components/LandingPageComponents/Footer";
import Image1 from "../../../../../public/Assets/Muse/Create Free New Year Card Templates Online.jpeg";

import Image2 from "../../../../../public/Assets/Muse/High-Quality New Year Card Templates.jpg";
import Image3 from "../../../../../public/Assets/Muse/Customize Your Free New Year Card Template.jpg";
import Image4 from "../../../../../public/Assets/Muse/Editable Happy New Year Poster Templates.jpg";
import Image5 from "../../../../../public/Assets/Muse/Create From Scratch Or Use Pre-Existing New Year Card Templates.jpg";
import Image6 from "../../../../../public/Assets/Muse/Downloadable Happy New Year Poster Templates.jpg";
import Image7 from "../../../../../public/Assets/Muse/Easily Save Your New Year Card Template.jpg";

import newImage2 from "../../../../../public/Assets/Muse/muse image-2.jpg";
import newImage1 from "../../../../../public/Assets/Muse/muse image-1.jpg";
import uploadIcon from "../../../../../public/Assets/Muse/cloud-upload.svg";
import exportIcon from "../../../../../public/Assets/Muse/export.svg";
import editIcon from "../../../../../public/Assets/Muse/edit-pencil.svg";
import newImage4 from "../../../../../public/Assets/Muse/new image.png";

const Newyear = () => {
  const svg = {
    leftArrow: (
      <svg
        width="14"
        height="10"
        viewBox="0 0 14 10"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75"
          stroke="black"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    ),
  };

  let data = [
    // {
    //   title: "Make Images Speak: Add Text to the Image and Craft Meaningful Stories",
    //   p: "Introducing Muse by Creatosaurus, the ultimate tool for adding text to images. Tell engaging stories and enhance your visuals effortlessly with our pro image text editor. With Creatosaurus, unleash your creativity and bring your images to life with just a few clicks. Try add text to photo now and experience the magic!",
    //   image: newImage2,
    // },
    {
      title:
        "Create Engaging Visual Stories: Add Text to Images Like a Pro with Muse by Creatosaurus",
      p: "With Creatosaurus' Muse Tool, you can become a master storyteller by adding text to your images. Unlock the potential of this innovative image text editor and effortlessly edit the text within your images. Captivate your audience with engaging visual stories, all accomplished with the expertise of a pro.",
      image: Image4,
    },
    {
      title: "Add custom text to photos: With fonts & styles ",
      p: "There are tons of font styles available for use. Add text to your image with creativity and tell the story with the help of your image. You can choose some of your favourite fonts in advertising, like Baskerville or more or you can customize your text colour, alignment and more with just a few clicks that make your image with text unique.",
      image: Image5,
    },
    {
      title: "Edit text in the image with customization",
      p: "Yes, you can edit or change various fonts or colours in your image and add free templates as well which boost your reach to the audience and make your task easier. So, what are waiting for edit text in your image with the help of Muse for free. Go now!",
      image: Image6,
    },
    {
      title: "Add text with blur, rotate, or more in the image",
      p: "Create unlimited custom new year greeting cards. Once you have finished creating your happy new year poster card, save your work. Muse stores all your new year wishes creations in one place —  revisit them in your dashboard 24/7.",
      image: Image7,
    },
    {
      title: "Add text to image templates",
      p: "If you're looking to add text to your images, we've got you covered! Our platform offers over 1000 free templates to choose from, making it easy to Edit text in the image to create professional-looking designs in no time. Get started with Muse now!",
      image: newImage1,
    },
    {
      title: "What’s different about Muse’s - Add text to image",
      p: "Simple interface and easy to use, No cost to use the platform, Accessible from anywhere with an internet connection, No unwanted advertisements or spam messages, No branding or watermarks added to your designs.",
      image: newImage4,
    },
    {
      title: "Muse - More than add text to image for your graphic design",
      p: "Discover the power of Muse, the free image editor that goes beyond text. Elevate your designs with icons, shapes, backgrounds, and more. Enjoy access to popular stock photo sites like Pixabay and Google Photos. Unlock limitless possibilities with Muse's pro set of tools for adding text to photos and beyond.",
      image: Image2,
    },
    {
      title: "How to Add Multiple Text in One Photo for Free",
      p: "Discover the magic of Muse: effortlessly add multiple text elements to any photo for free. With Muse's intuitive interface, you can edit text in the image, customize fonts, and enhance your visuals. Unlock endless creative possibilities and captivate your audience with stunning text-enhanced photos.Add text to photos now!",
      image: Image2,
    },
    {
      title:
        "Get Creative with 1000+ Templates & Assets: Ignite Your Imagination",
      p: `"Add Text to Images and Ignite Your Imagination: Explore 1000+ Templates & Assets to Get Creative. Unlock a world of possibilities and effortlessly enhance your visuals with captivating text using our extensive collection of templates and assets."
Enhance your work with the help of our image text editor.`,
      image: Image2,
    },
  ];

  let faq = [
    {
      id: 1,
      title: "Can I add text to image online for free?",
      content:
        "Yes, Add text to image effortlessly with our image text editor online for free..",
    },
    {
      id: 2,
      title: "Add text to image without losing quality?",
      content:
        "Yes, you can add text to photo without losing quality with the help of the Muse graphic design tool.",
    },
    {
      id: 3,
      title: "How to insert an image in a text editor?",
      content: `First, insert your desired image. Next, either tap on the "Text option" or select the Text Style you prefer. Then, simply write your desired text. It's that easy!`,
    },
    {
      id: 4,
      title: " How to edit the text in the image?",
      content:
        "You can edit or change various fonts or colours of the text in your image and add free templates which will boost your reach to the audience and make your design beautiful. ",
    },
    {
      id: 5,
      title: "What is Muse by Creatosaurus?",
      content:
        "Enhance photos effortlessly with Muse, Creatosaurus' powerful yet user-friendly editing tool. Crop, resize, adjust colors, add filters, text, and more. Elevate your photos with ease.",
    },
  ];

  const openUrl = (url) => {
    window.open(url);
  };

  return (
    <div>
      <Head>
        <link rel="icon" href="/Assets/creatosaurusfavicon.ico" />
        <title>Add text to image | Free Online Image Text Editor</title>
        <meta
          name="title"
          content="Add text to image | Free Online Image Text Editor"
        />
        <meta
          name="description"
          content="Add text to photos online for free. Enhance your graphic with customization to edit text in the image, fonts, and styles effortlessly for free with Muse by Creatosaurus."
        />
        <meta
          property="og:title"
          content="Add text to image | Free Online Image Text Editor"
          key="title"
        />
        <meta
          property="og:description"
          content="Add text to photos online for free. Enhance your graphic with customization to edit text in the image, fonts, and styles effortlessly for free with Muse by Creatosaurus."
          key="description"
        />
      </Head>
      <NavigationBar />

      <div className={museStyle.top_block}>
        <div className={museStyle.dataBlocks_dataBox_reverse}>
          <div className={museStyle.blockImage} style={{ boxShadow: "none" }}>
            <Image
              src={newImage2}
              alt="Create Free New Year Card Templates Online. Customizable & Printable 2023 New Year Graphic Templates"
            />
          </div>
          <div className={museStyle.blockText}>
            {/**
                             * <div className={museStyle.blockText_icon}>
                            <Image src={logo} alt='Failed to load image' />
                        </div>
                             */}
            <div className={museStyle.blockText_title}>
              <h1>
                Make Images Speak: Add Text to the Image and Craft Meaningful
                Stories
              </h1>
            </div>
            <div className={museStyle.blockText_description}>
              <p>
                Introducing Muse by Creatosaurus, the ultimate tool for adding
                text to images. Tell engaging stories and enhance your visuals
                effortlessly with our pro image text editor. With Creatosaurus,
                unleash your creativity and bring your images to life with just
                a few clicks. Try add text to photo now and experience the
                magic!
              </p>
            </div>
            <div className={museStyle.blockText_button}>
              <button
                onClick={() => {
                  openUrl(
                    "https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&category=new%20year&app=muse"
                  );
                }}
              >
                Get Started For Free
              </button>
            </div>
          </div>
        </div>
      </div>

      {/* <div className={museStyle.videoContainer}>
          <iframe
            className={museStyle.videoBlock}
            src="https://www.youtube.com/embed/eS5tpAUEuzA?start=492"
            title="YouTube video player"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </div> */}

      <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
        {data.map((res, index) => {
          return (
            <div
              className={
                index % 2 == 0
                  ? museStyle.dataBlocks_dataBox
                  : museStyle.dataBlocks_dataBox_reverse
              }
              key={`blocks-data-muse-${index}`}
            >
              <div className={museStyle.blockImage}>
                <Image src={res.image} alt={res.title} />
              </div>
              <div className={museStyle.blockText}>
                <div className={museStyle.blockText_title}>
                  <h1>{res.title}</h1>
                </div>
                <div className={museStyle.blockText_description}>
                  <p>{res.p}</p>
                </div>
                <div className={museStyle.blockText_button}>
                  <button
                    onClick={() => {
                      openUrl(
                        "https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&category=new%20year&app=muse"
                      );
                    }}
                  >
                    Get Started For Free
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>

      <div className={museStyle.redPoster}>
        <div className={museStyle.redPosterContainer}>
          <div className={museStyle.redPosterTop}>
            <div className={museStyle.redPosterHeading}>
              <h1>
                Muse - More than add text to image for your graphic design
              </h1>
            </div>
            <div className={museStyle.redPosterPara}>
              <p>
                Discover the power of Muse, the free image editor that goes
                beyond text. Elevate your designs with icons, shapes,
                backgrounds, and more. Enjoy access to popular stock photo sites
                like Pixabay and Google Photos. Unlock limitless possibilities
                with Muse's pro set of tools for adding text to photos and
                beyond.
              </p>
            </div>
            <div className={museStyle.redPosterButton}>
              <button
                onClick={() => openUrl("https://www.muse.creatosaurus.io/")}
              >
                Get Started for Free {svg.leftArrow}
              </button>
            </div>
          </div>
          <div className={museStyle.redPosterBottom}>
            <Image src={imgSrc} alt="muse_img loading..." />
          </div>
        </div>
      </div>

      <div className={museStyle.how_to_container}>
        <h1>How to add text to photo online</h1>
        <div className={museStyle.card_container}>
          <div className={museStyle.card}>
            <Image src={uploadIcon} alt="Upload Image" /> <br />
            <h4>Step 1 - Upload a image</h4>
            <p>
              Upload a image or choose from our free stock library (more than
              1000 are available) for free.
            </p>
          </div>
          <div className={museStyle.card}>
            <Image src={editIcon} alt="Upload Image" /> <br />
            <h4>2. Add and style text</h4>
            <p>
              Add text to the top of the image and customize it according to
              your need.
            </p>
          </div>
          <div className={museStyle.card}>
            <Image src={exportIcon} alt="Upload Image" /> <br />
            <h4>3. Export and share</h4>
            <p>Then export and download the image in high quality.</p>
          </div>
        </div>
      </div>

      <div className={museStyle.discover_apps_container}>
        <div className={museStyle.more_apps_container}>
          <h1>Discover more similar features like add to text image</h1>
          <div className={museStyle.button_container}>
            {apps.map((data, index) => {
              return (
                <button
                  key={index}
                  onClick={() => {
                    window.open(data.url);
                  }}
                >
                  {data.title}
                </button>
              );
            })}
          </div>
        </div>
      </div>

      <div className={museStyle.questionsContainer}>
        <div className={museStyle.heading}>
          <span>Frequently Asked Questions</span>
        </div>
        {faq.map(({ title, content, id }) => (
          <FrequentlyAskedQuestions title={title} content={content} key={id} />
        ))}
      </div>

      <FifthBlackSection />
      <Footer />
    </div>
  );
};

export default Newyear;
