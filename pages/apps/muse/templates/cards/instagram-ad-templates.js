import React from 'react'
import Head from 'next/head';
import Image from 'next/image';
import museStyle from '../../../../../styles/muse.module.css';
import NavigationBar from '../../../../../Components/LandingPageComponents/NavigationBar';
import imgSrc from '../../../../../public/Assets/muse_1.png';
import apps from "../../../../../appsInfo";
import FrequentlyAskedQuestions from '../../../../../Components/FaqSection';
import FifthBlackSection from '../../../../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../../../../Components/LandingPageComponents/Footer';
import Image1 from '../../../../../public/Assets/Muse/Create Free New Year Card Templates Online.jpeg'

import Image2 from '../../../../../public/Assets/Muse/High-Quality New Year Card Templates.jpg'
import Image3 from '../../../../../public/Assets/Muse/Customize Your Free New Year Card Template.jpg'
import Image4 from '../../../../../public/Assets/Muse/Editable Happy New Year Poster Templates.jpg'
import Image5 from '../../../../../public/Assets/Muse/Create From Scratch Or Use Pre-Existing New Year Card Templates.jpg'
import Image6 from '../../../../../public/Assets/Muse/Downloadable Happy New Year Poster Templates.jpg'
import Image7 from '../../../../../public/Assets/Muse/Easily Save Your New Year Card Template.jpg'



const InstagramAdTemplates = () => {

    const svg = {
        leftArrow: (
            <svg
                width='14'
                height='10'
                viewBox='0 0 14 10'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
            >
                <path
                    d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
                    stroke='black'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                />
            </svg>
        ),
    };


    let data = [
        {
            title: "1000+ Instagram Ad Templates",
            p: "Muse Instagram Ad Creator unfolds a treasure trove of compelling, professional 1000+ ad templates — kickstart your Instagram ad creation journey by picking a template that captures the essence, tone, graphics, and theme you want your ad to convey.",
            image: Image2
        }, {
            title: "Customize Your Instagram Ad Template",
            p: "Inject your Instagram ad template with some personality. Here’s how you can — input ad copy text optimized to your brand message. Play around with fonts carrying everything from a sleek to a quirky feel. Throw in images, icons, and fun graphic elements. Place a compelling CTA anywhere in the template.",
            image: Image3
        }, {
            title: "Royalty-Free Stock Photos",
            p: "Your Instagram ad got to paint a compelling graphic portrait. After all, we are talking about Instagram — a platform thriving on visual content. Browse through millions of royalty-free, high-resolution stock images by Muse — to land on the one that will seamlessly blend into your Instagram ad post.",
            image: Image4
        }, {
            title: "Create Instagram Ad Personalized To Your Brand",
            p: "Make any Instagram ad template you choose unique to your brand. Upload your logo, slogan, tagline, or images exclusive to your business offerings on your Muse dashboard — drag and drop them to the right spots on the template. Opt for colours and typography aligned with your brand identity.",
            image: Image5
        }, {
            title: "Create Instagram Ad From Scratch Or Build With Existing Templates",
            p: "Whether you are clueless or have the look and feel of your Instagram ad all charted out — Muse Instagram Ad Maker makes room for both. Unleash your creative brainstorming on an in-built artboard from template creation to personalization. If not, make your work easy with premade Muse templates.",
            image: Image6
        }, {
            title: "Fuss-Free Editing Tools",
            p: "Give an Instagram ad post template your creative spin — whether you are a seasoned designer or a complete novice. Finetune its colours and layout settings. A broad spectrum of photo effects is at your fingertips — let your images throb with a rich aesthetic appeal by adjusting their depth and texture.",
            image: Image7
        }, {
            title: "Instagram Ad Creator For Any Business",
            p: "A full-fledged corporation, a startup, or a small business — Muse scales and simplifies Instagram ad creation workflow for all. Create customized Instagram advertisements using free templates in a few clicks — tap into the audience targeting and brand-building potential of Instagram like never before.",
            image: Image2
        }, {
            title: "Save Your Instagram Ad On The Go",
            p: "Once you have created your Instagram ad, click on save to revisit your creation 24/7 in your Muse dashboard. Repurpose any Instagram ad post and tailor it for future paid marketing campaigns across diverse social media channels — by using the editing toolbar to alter its layout dimensions.",
            image: Image3
        }, {
            title: "Share Your Instagram Ad — Right Away Or Later",
            p: "Once you have crafted your Muse Instagram ad post template to perfection, share your creation in one click and let it work its magic on your audience. You can also schedule your Instagram ad post sharing — by integrating your Instagram business account into our social media management tool Cache.",
            image: Image4
        } , {
            title: "Create Instagram Ads For All Kinds Of Static Image Posts",
            p: "With Muse Instagram Ad Creator, gain access to tons of templates in the square, horizontal, and vertical formats. Customize the layout of your favourite template to tailor it for any graphic Instagram ad you wish to make — be it a story ad, image ad, explore ad, collection ad post, or shopping ad.",
            image: Image5
        } 
    ]


    let faq = [
        {
            id: 1,
            title: "Are Muse Instagram Ads Design Templates free to use?",
            content: "Access 1000+ post templates, stock images, & a wide range of graphic design tools with a free plan. Upgrade to paid plans for extra features.",
        }, {
            id: 2,
            title: "Who can use Muse Instagram Ad Creator?",
            content: "Muse Instagram Ad Maker is a free online design tool for marketers, graphic designers, content creators, and students.",
        }, {
            id: 3,
            title: "What can I do with Muse Instagram Ad Template Maker?",
            content: "Muse Instagram Ad Post Template Tool is ideal for creating image posts for your feed, story, explore, shopping, and collection section.",
        }, {
            id: 4,
            title: "Can I use Muse to create video-based carousel ads and IGTV ads?",
            content: "The use case of Muse Instagram Ad Creator is limited to static posts. Video ad creation is in our pipeline, so stay tuned for future updates!",
        } , {
            id: 5,
            title: "Can I save the ad posts I create with Muse Instagram Ad Maker?",
            content: "Muse Instagram Ad Template Tool lets you save your customized Instagram ad post creations in an easy-to-access folder — just one click away.",
        }
    ]

    const openUrl = (url) => {
        window.open(url)
    }

    return (
        <div>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>Free Customizable Instagram Ad Templates | Muse</title>
                <meta
                    name="title"
                    content="Free Customizable Instagram Ad Templates | Muse"
                />
                <meta
                    name="description"
                    content="Create Instagram ads that amplify the reach and engagement of your business with 1000+ customizable, eye-catching free Instagram ad templates offered by Muse."
                />
                <meta
                    property="og:title"
                    content="Free Customizable Instagram Ad Templates | Muse"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Create Instagram ads that amplify the reach and engagement of your business with 1000+ customizable, eye-catching free Instagram ad templates offered by Muse."
                    key="description"
                />
            </Head>
            <NavigationBar />

            <div className={museStyle.top_block}>
                <div className={museStyle.dataBlocks_dataBox_reverse}>
                    <div className={museStyle.blockImage} style={{ boxShadow: 'none' }}>
                        <Image src={Image1} alt='Create Free New Year Card Templates Online. Customizable & Printable 2023 New Year Graphic Templates' />
                    </div>
                    <div className={museStyle.blockText}>
                        {
                            /**
                             * <div className={museStyle.blockText_icon}>
                            <Image src={logo} alt='Failed to load image' />
                        </div>
                             */
                        }
                        <div className={museStyle.blockText_title}>
                            <h1>Muse Instagram Ad Creator - Make The Scrolling Screech To A Halt</h1>
                        </div>
                        <div className={museStyle.blockText_description}>
                            <p>
                                Let alone popping up on people’s Instagram feeds — give your ad a chance to steal the show with Muse Instagram ad templates tailored to your brand.
                            </p>
                        </div>
                        <div className={museStyle.blockText_button}>
                            <button onClick={() => {
                                openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&category=ads&app=muse");
                            }}>Get Started For Free</button>
                        </div>
                    </div>
                </div>
            </div>



            <div className={museStyle.videoContainer}>
                <iframe
                    className={museStyle.videoBlock}
                    src='https://www.youtube.com/embed/eS5tpAUEuzA?start=492'
                    title='YouTube video player'
                    allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                    allowFullScreen
                ></iframe>
            </div>



            <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h1>{res.title}</h1>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button onClick={() => {
                                        openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&category=ads&app=muse");
                                    }}>Get Started For Free</button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.redPoster}>
                <div className={museStyle.redPosterContainer}>
                    <div className={museStyle.redPosterTop}>
                        <div className={museStyle.redPosterHeading}>
                            <h1>Free online graphic design post maker and creator</h1>
                        </div>
                        <div className={museStyle.redPosterPara}>
                            <p>
                                Create graphic design posts for Instagram, Facebook, and other social media platforms at scale with our massive library of templates with our easy-to-use design editor.
                            </p>
                        </div>
                        <div className={museStyle.redPosterButton}>
                            <button onClick={() => openUrl("https://www.muse.creatosaurus.io/")}>Get Started for Free {svg.leftArrow}</button>
                        </div>
                    </div>
                    <div className={museStyle.redPosterBottom}>
                        <Image src={imgSrc} alt='muse_img loading...' />
                    </div>
                </div>
            </div>



            <div className={museStyle.how_to_container}>
                <h1>Steps To Create An Instagram Ad With Muse Instagram Ad Creator</h1>
                <div className={museStyle.card_container}>
                    <div className={museStyle.card}>
                        <p>1. Select an Instagram ad template or use the Muse artboard to create your own.</p>
                    </div>
                    <div className={museStyle.card}>
                        <p>2. Remove or replace existing text and visual elements on your Instagram ad post template — customize it by inputting fonts, text, images, icons, colour combinations, and graphic components that resonate with your brand personality.</p>
                    </div>
                    <div className={museStyle.card}>
                        <p>3. Drop in branded elements like your logo, ad copy, slogan, tagline, and product images anywhere on the template.</p>
                    </div>
                    <div className={museStyle.card}>
                        <p>4. Edit your images and make them look graphically powerful with the right shades, depth, and feel — unlock an impressive number of photo effects customization options through the editing toolbar.</p>
                    </div>
                </div>
            </div>


            <div className={museStyle.discover_apps_container}>
                <div className={museStyle.more_apps_container}>
                    <h1>Discover more:</h1>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <button key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</button>;
                        })}
                    </div>
                </div>
            </div>


            <div className={museStyle.questionsContainer}>
                <div className={museStyle.heading}>
                    <span>Frequently Asked Questions</span>
                </div>
                {faq.map(({ title, content, id }) => (
                    <FrequentlyAskedQuestions title={title} content={content} key={id} />
                ))}
            </div>

            <FifthBlackSection />
            <Footer />
        </div>
    )
}

export default InstagramAdTemplates