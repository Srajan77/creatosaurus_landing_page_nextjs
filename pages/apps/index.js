import React from "react";
import Head from "next/head";
import styles from "../../styles/Apps.module.css";
import Navbar from "../../Components/LandingPageComponents/NavigationBar";
import Footer from "../../Components/LandingPageComponents/Footer";
import FifthBlackSection from "../../Components/LandingPageComponents/FifthBlackSection";
import Link from "next/link";
import Image from "next/image";

import Cache from "../../public/Assets/Apps/cache.png";
import Captionater from "../../public/Assets/Apps/captionater.png";
import Muse from "../../public/Assets/Apps/muse.png";
import Hashtag from "../../public/Assets/Apps/hashtag.png";
import Quotes from "../../public/Assets/Apps/quotes.png";
import Noice from "../../public/Assets/Apps/noise.png";
import Appolo from "../../public/Assets/Apps/apollo.png";
import Steno from "../../public/Assets/Apps/Steno.jpg";

// apps
import Facebook from "../../public/Assets/Apps/facebook.png";
import Instagram from "../../public/Assets/Apps/instagram.png";
import LinkedIn from "../../public/Assets/Apps/linkedIn.png";
import Tumbler from "../../public/Assets/Apps/tumbler.png";
import Snapchat from "../../public/Assets/Apps/snapchat.png";
import Youtube from "../../public/Assets/Apps/youtube.png";
import Pinterest from "../../public/Assets/Apps/pinterest.png";

// 2
import Unsplash from '../../public/Assets/Apps/2unsplash.png';
import GoogleFonts from '../../public/Assets/Apps/2googlefonts.svg';
import Pixabay from '../../public/Assets/Apps/2pixabay.png';
import Pexels from '../../public/Assets/Apps/2pexels.png';
import OpenMoji from '../../public/Assets/Apps/2openmoji.png';
import Tenor from '../../public/Assets/Apps/2tenor.png';
import Giphy from '../../public/Assets/Apps/giphy.svg';
import Twemoji from '../../public/Assets/Apps/2twemoji.png';

// 3
import Drive from "../../public/Assets/Apps/3drive.png";
import Photo from "../../public/Assets/Apps/3photo.png";
import Twitter from "../../public/Assets/Apps/3twitter.png";
import Clearbit from "../../public/Assets/Apps/3clearbit.png";
import Dropbox from "../../public/Assets/Apps/3dropbox.svg";
import Bitmoji from "../../public/Assets/Apps/3bitmoji.png";
import Box from "../../public/Assets/Apps/3box.png";

const Apps = () => {
  const creatosaurusOriginals = [
    {
      title: "Cache",
      desc: "Manage and schedule posts",
      image: Cache,
      subTitle: "Schedule Post",
      color: "#0078FF",
      link: "https://cache.creatosaurus.io",
    },
    {
      title: "Muse",
      desc: "Design graphics with templates",
      image: Muse,
      subTitle: "Design Graphics",
      color: "#FF8A25",
      link: "https://muse.creatosaurus.io",
    },
    {
      title: "Captionator",
      desc: "Generate content with AI writer",
      image: Captionater,
      subTitle: "AI Writer",
      color: "#00ECC2",
      link: "https://captionator.creatosaurus.io",
    },
    {
      title: "Steno",
      desc: "Easy text editor & AI Blog Writer",
      image: Steno,
      subTitle: "Text Editor",
      color: "#1410C7",
      link: "https://steno.creatosaurus.io",
    },
    {
      title: "Apollo",
      desc: "Easy video editor (coming soon)",
      image: Appolo,
      subTitle: "Video Editor",
      color: "#4267B2",
      link: "",
    },
    {
      title: "Quotes",
      desc: "Search & save quotes, authors and more",
      image: Quotes,
      subTitle: "Curate Quotes",
      color: "#FFD75F",
      link: "https://quotes.creatosaurus.io",
    },
    {
      title: "#Tags",
      desc: "Create & manage hashtag groups",
      image: Hashtag,
      subTitle: "Hashtag Manager",
      color: "#FF43CA",
      link: "https://hashtags.creatosaurus.io",
    },
    {
      title: "Noice",
      desc: 'CC0 assets that will make you say "Nice!"',
      image: Noice,
      subTitle: "Free Stock Assets",
      color: "#009B77",
      link: "https://www.noice.creatosaurus.io/",
    }
  ];

  const socialPlatform = [
    {
      style: { height: 34, width: 34 },
      image: Facebook,
      title: "Facebook",
      desc: "Social media and networking platform",
      color: "#0165E1",
    },
    {
      style: { height: 30, width: 30 },
      image: Instagram,
      title: "Instagram",
      desc: "Photo and video sharing app",
      color: "#E34576",
    },
    {
      style: { height: 30, width: 29.35 },
      image: LinkedIn,
      title: "LinkedIn",
      desc: "Work focused social networking",
      color: "#2867B2",
    },
    {
      style: { height: 34, width: 34 },
      image: Tumbler,
      title: "Tumblr",
      desc: "Blogging and social networking",
      color: "#395976",
    },
    {
      style: { height: 36, width: 54 },
      image: Snapchat,
      title: "Snapchat",
      desc: "Instant messaging app (coming soon)",
      color: "#FFFC00",
    },
    {
      style: { height: 22.14, width: 32 },
      image: Youtube,
      title: "YouTube",
      desc: "Video sharing and social networking",
      color: "#FF0000",
    },
    {
      style: { height: 42, width: 42 },
      image: Pinterest,
      title: "Pinterest",
      desc: "Visual pin sharing and social networking",
      color: "#BD081C",
    },
  ];

  const assetPlatform = [
    {
      style: { height: 36, width: 36 },
      image: Unsplash,
      title: "Unsplash",
      desc: "The internet’s source of freely-usable images",
      color: "#000000",
    },
    {
      style: { height: 23.78, width: 42 },
      image: GoogleFonts,
      title: "Google Fonts",
      desc: "Open typography and iconography",
      color: "#E8F0FE",
    },
    {
      style: { height: 30, width: 30 },
      image: Pixabay,
      title: "Pixabay",
      desc: "Stunning free images & royalty free stock",
      color: "#48A947",
    },
    {
      style: { height: 28, width: 28 },
      image: Pexels,
      title: "Pexels",
      desc: "The best free stock photos and videos",
      color: "#05A081",
    },
    {
      style: { height: 36, width: 36 },
      image: OpenMoji,
      title: "OpenMoji",
      desc: "Open source emojis for everyone (Coming soon)",
      color: "#3D98BB",
    },
    {
      style: { height: 30, width: 30 },
      image: Tenor,
      title: "Tenor",
      desc: "Find the perfect Animated GIFs",
      color: "#006CD5",
    },
    {
      style: { height: 34, width: 34 },
      image: Twemoji,
      title: "Twemoji",
      desc: "Twitter’s open source emojis for everyone (Coming soon)",
      color: "#00A2F5",
    },
    {
      style: { height: 30, width: 30 },
      image: Giphy,
      title: "Giphy",
      desc: "Best & newest GIFs & Animated Stickers",
      color: "#000000",
    },
  ];

  const tools = [
    {
      style: { height: 28.68, width: 32 },
      image: Drive,
      title: "Google Drive",
      desc: "Cloud Storage for Work and Home",
      color: "#E8F0FE",
    },
    {
      style: { height: 44, width: 44 },
      image: Photo,
      title: "Google Photos",
      desc: "Home for all your photos and videos",
      color: "#E8F0FE",
    },
    {
      style: { height: 23.49, width: 42 },
      image: Twitter,
      title: "Twitter Trends",
      desc: "Trending topics and hashtags on Twitter",
      color: "#00A2F5",
    },
    {
      style: { height: 32.55, width: 34 },
      image: Clearbit,
      title: "Clearbit Brand Logo",
      desc: "Find Company Logos by Clearbit",
      color: "#273266",
    },
    {
      style: { height: 32, width: 29.76 },
      image: Dropbox,
      title: "Dropbox",
      desc: "For your content and workflows",
      color: "#007EE5",
    },
    {
      style: { height: 30, width: 30 },
      image: Bitmoji,
      title: "Bitmoji",
      desc: "Your own personal emoji, cartoon avatar",
      color: "#39CB8D",
    },
    {
      style: { height: 16.1, width: 30 },
      image: Box,
      title: "Box",
      desc: "Secure Cloud Content Management (Coming soon)",
      color: "#0071F7",
    },
  ];
  return (
    <div>
      <Head>
        <link rel="icon" href="/Assets/creatosaurusfavicon.ico" />
        <title>Creatosaurus | Apps</title>
      </Head>
      <Navbar />
      <header className={styles.mainContainer}>
        <div className={styles.headingSection}>
          <h1>Don&apos;t blend in.Tell your story.</h1>
          <p>Personalized apps designed for creators like you.</p>
          <div>
            <a href="https://www.app.creatosaurus.io/signup">
              <button>Get Started—It&apos;s Free</button>
            </a>
          </div>
        </div>
        <div className={styles.appsCardBlock}>
          <h1>Powerful apps for your stories.</h1>
          <p>
            The world&apos;s biggest influencers, creators, publishers, startups
            and brands use
            <br />
            Creatosaurus in their marketing strategy.
          </p>

          <div className={styles.scroll_container_main} style={{ marginTop: "30px" }}>
            <div className={styles.head}>
              <h4>Creatosaurus Originals</h4>
              <div>
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.5 5L7.5 10L12.5 15"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M7.5 5L12.5 10L7.5 15"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </div>
            <div className={styles.scroll_container}>
              {creatosaurusOriginals.map((res, index) => {
                return (
                  <div className={styles.app} key={`${res.title}-${index}`}>
                    <div
                      className={styles.app_box}
                      style={{ backgroundColor: res.color }}
                    >
                      <span>{res.subTitle}</span>
                      <Image src={res.image} alt="" />
                    </div>
                    <span className={styles.app_name}>{res.title}</span>
                    <span className={styles.app_info}>{res.desc}</span>
                  </div>
                );
              })}
            </div>
          </div>
          <div className={styles.scroll_container_main}>
            <div className={styles.head}>
              <h4>Social Media Platforms</h4>
              <div>
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.5 5L7.5 10L12.5 15"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M7.5 5L12.5 10L7.5 15"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </div>
            <div className={styles.scroll_container}>
              {socialPlatform.map((res, index) => {
                return (
                  <div className={styles.app} key={`${res.title}-${index}`}>
                    <div
                      className={styles.app_box}
                      style={{ backgroundColor: res.color }}
                    >
                      <div className={styles.circle}>
                        <Image src={res.image} alt="" />
                      </div>
                    </div>
                    <span className={styles.app_name}>{res.title}</span>
                    <span className={styles.app_info}>{res.desc}</span>
                  </div>
                );
              })}
            </div>
          </div>
          <div className={styles.scroll_container_main}>
            <div className={styles.head}>
              <h4>Stock Asset Platforms</h4>
              <div>
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.5 5L7.5 10L12.5 15"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M7.5 5L12.5 10L7.5 15"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </div>
            <div className={styles.scroll_container}>
              {assetPlatform.map((res, index) => {
                return (
                  <div className={styles.app} key={`${res.title}-${index}`}>
                    <div
                      className={styles.app_box}
                      style={{ backgroundColor: res.color }}
                    >
                      <div className={styles.circle}>
                        <Image src={res.image} alt="" />
                      </div>
                    </div>
                    <span className={styles.app_name}>{res.title}</span>
                    <span className={styles.app_info}>{res.desc}</span>
                  </div>
                );
              })}
            </div>
          </div>
          <div className={styles.scroll_container_main}>
            <div className={styles.head}>
              <h4>Tools</h4>
              <div>
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.5 5L7.5 10L12.5 15"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M7.5 5L12.5 10L7.5 15"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </div>
            <div className={styles.scroll_container}>
              {tools.map((res, index) => {
                return (
                  <div className={styles.app} key={`${res.title}-${index}`}>
                    <div
                      className={styles.app_box}
                      style={{ backgroundColor: res.color }}
                    >
                      <div className={styles.circle}>
                        <Image src={res.image} alt="" />
                      </div>
                    </div>
                    <span className={styles.app_name}>{res.title}</span>
                    <span className={styles.app_info}>{res.desc}</span>
                  </div>
                );
              })}
            </div>
          </div>
        </div>

        <FifthBlackSection />
        {/* <div className={styles.fifthBlackSection}>
          <h1>
            You focus on telling stories,
            <br />
            we do everything else.
          </h1>
          <button>Signup for Free</button>
        </div> */}
      </header>
      <Footer />
    </div>
  );
};

export default Apps;
