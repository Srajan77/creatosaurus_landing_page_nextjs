import React from 'react'
import style from "../../../styles/top100quotes.module.css"
import Navbar from "../../../Components/LandingPageComponents/NavigationBar";
import Footer from "../../../Components/LandingPageComponents/Footer"
import axios from 'axios';

export const getServerSideProps = async () => {
    const response = await axios.get("https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/top100quotes")
    const quotes = response.data

    if (!quotes) {
        return {
            notFound: true,
        };
    }
    return {
        props: {
            quotes,
        },
    };
};


const QuoteOfTheDay = (quotes) => {
    // Need More Information
    const NeedMoreInformation = () => {
        return (
            <div className={style.fifthBlackSection}>
                <h1>
                    You focus on telling stories,
                    <br />
                    we do everything else.
                </h1>
                <button>Signup for Free</button>
            </div>
        );
    };

    const actualQuotes = () => {
        return (
            <div className={style.container}>
                <div className={style.quotesContainer}>
                    <h1>
                        100 Top Quotes
                    </h1>
                </div>
                {quotes.quotes.map((quote, index) => {
                    return (
                        <div className={style.quotesContainer} key={index}>
                            <span>{quote.Queote}</span>
                            <h2>{quote.Author}</h2>
                        </div>
                    )
                })}
            </div>
        )
    }

    return (
        <div>
            <Navbar />
            <div className={style.headingButton}>
                <button>Free quote poster maker. Thousands of beautiful quote templates <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0_5605_1628)">
                        <path d="M1.57227 7.5H13.1348M13.1348 7.5L9.38476 3.75M13.1348 7.5L9.38476 11.25" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0_5605_1628">
                            <rect width="15" height="15" fill="white" transform="translate(0.5)" />
                        </clipPath>
                    </defs>
                </svg></button>
            </div>
            {actualQuotes()}
            {NeedMoreInformation()}
            <Footer />
        </div>
    );
}

export default QuoteOfTheDay;