import React from 'react';
import style from '../../../../styles/top100quotes.module.css';
import Navbar from '../../../../Components/LandingPageComponents/NavigationBar';
import Footer from '../../../../Components/LandingPageComponents/Footer';
import { getAuthorQuotes } from '../../../../lib/getQuotesOfAuthor';
import { useRouter } from 'next/dist/client/router';
import Masonry from 'react-responsive-masonry';
import Save from '../../../../public/Assets/Save.svg';
import Copy from '../../../../public/Assets/Copy.svg';
import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';

export const getServerSideProps = async (context) => {
  const search = JSON.stringify(context.query.topic)
    .replace(/-/g, ' ')
    .replace(/quotes/g, '');
  const quotes = await getAuthorQuotes(search.replace(/"/g, ''));

  if (!quotes) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      quotes,
    },
  };
};

const SingleTopicQuotes = (quotes) => {
  const router = useRouter();
  const data = router.query;

  // Need More Information
  const NeedMoreInformation = () => {
    return (
      <div className={style.fifthBlackSection}>
        <h1>
          You focus on telling stories,
          <br />
          we do everything else.
        </h1>
        <button>Signup for Free</button>
      </div>
    );
  };

  const actualQuotes = () => {
    const topic_data =
      Object.keys(data)[0] === 'Tags' ? Object.keys(data)[0] : data.topic;
    const without_hyphen = topic_data.replace(/-/g, ' ');
    const topic = without_hyphen.replace(/quotes/g, '');
    return (
      <div className={style.container}>
        <div className={style.quotesContainer}>
          <h1>{topic} Quotes</h1>
        </div>
      </div>
    );
  };

  const authorQuotes = () => {
    return (
      <div className={style.searchContainerCenter}>
        <div style={{ overflow: 'scroll' }}>
          <Masonry columnsCount={3} gutter={20}>
            {quotes.quotes.map((data, index) => {
              const fiveWords = data.Queote.split(' ').slice(0, 5).join(' ');
              const slug = slugify(fiveWords);
              const trimedAuthor = data.Author.includes(",") ? data.Author.substring(
                0,
                data.Author.indexOf(',')
              ) : data.Author;
              const slugAuthor = slugify(trimedAuthor);
              const author = slugAuthor.substring(0, 30);

              return (
                <Link
                  href={{
                    pathname: '/apps/quotes/[slug]',
                    query: data,
                  }}
                  as={`/apps/quotes/${slug}-quote-by-${author}-${data._id}`}
                  key={data._id}
                  passHref
                >
                  <div
                    className={style.card}
                    style={{ cursor: 'pointer' }}
                    key={index}
                  >
                    <p>{data.Queote}</p>
                    <div className={style.authorContainer}>
                      <span>- {data.Author}</span>
                      <div className={style.functions}>
                        <Image
                          style={{ cursor: 'pointer' }}
                          src={Save}
                          alt=''
                        />
                        <Image
                          style={{ cursor: 'pointer' }}
                          src={Copy}
                          alt=''
                          onClick={() => coptText(data.Queote, data.Author)}
                        />
                      </div>
                    </div>
                  </div>
                </Link>
              );
            })}
          </Masonry>
        </div>
      </div>
    );
  };

  const trimedAuthor = Object.keys(data)[0];
  const title = trimedAuthor + ' Quotes' + ' - Creatosaurus';
  const description =
    'Explore thousands of ' +
    trimedAuthor +
    ' Quotes by hundreds of authors at Creatosaurus. Browse the top quotations on  ' +
    trimedAuthor +
    '.';

  const slugify = (string) => {
    return string
      .toString()
      .trim()
      .toLowerCase()
      .replace(/\s+/g, '-')
      .replace(/[^\w\-]+/g, '')
      .replace(/\-\-+/g, '-')
      .replace(/^-+/, '')
      .replace(/-+$/, '');
  };

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name='description' content={description} />
        <meta property='og:title' content={title} key='title' />
        <meta
          property='og:description'
          content={description}
          key='description'
        />

        <meta property='og:site_name' content='Quotes by Creatosaurus'></meta>
        <meta
          property='og:url'
          content={`https://www.creatosaurus.io${router.asPath}`}
          key='url'
        />

        {/* Twitter */}
        <meta name='twitter:title' content={title} />
        <meta name='twitter:description' content={description} />

        <meta name='twitter:card' content='summary_large_image' />
        <meta name='twitter:site' content='@creatosaurushq' />
        <meta
          name='twitter:url'
          content={`https://www.creatosaurus.io${router.asPath}`}
        />
      </Head>
      <Navbar />
      <div className={style.headingButton}>
        <button>
          Free quote poster maker. Thousands of beautiful quote templates{' '}
          <svg
            width='16'
            height='15'
            viewBox='0 0 16 15'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
          >
            <g clipPath='url(#clip0_5605_1628)'>
              <path
                d='M1.57227 7.5H13.1348M13.1348 7.5L9.38476 3.75M13.1348 7.5L9.38476 11.25'
                stroke='white'
                strokeWidth='1.5'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
            </g>
            <defs>
              <clipPath id='clip0_5605_1628'>
                <rect
                  width='15'
                  height='15'
                  fill='white'
                  transform='translate(0.5)'
                />
              </clipPath>
            </defs>
          </svg>
        </button>
      </div>
      {actualQuotes()}
      {authorQuotes()}
      {NeedMoreInformation()}
      <Footer />
    </div>
  );
};

export default SingleTopicQuotes;
