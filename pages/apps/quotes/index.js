import React, { useState } from 'react';
import style from '../../../styles/Quotes.module.css';
import Navbar from '../../../Components/LandingPageComponents/NavigationBar';
import Image from 'next/image';
import banner from '../../../public/Assets/headerimage.png';
import DownArrow from '../../../public/Assets/DownArrow.svg';
import Rating from '../../../public/Assets/Rating.svg';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Link from 'next/link';
import axios from 'axios';
import Masonry from 'react-responsive-masonry';
import Save from '../../../public/Assets/Save.svg';
import Copy from '../../../public/Assets/Copy.svg';
import QuoteBanner from '../../../public/Assets/QuoteBanner.png';
import { toast } from 'react-toastify';

export const getServerSideProps = async () => {
  const response = await axios.get(
    'https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/quotesoftheday'
  );
  const quotesdata = response.data;

  if (!quotesdata) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      quotesdata,
    },
  };
};

const Quotes = (quotesdata) => {
  const [search, setSearch] = useState('');
  const [quotes, setQuotes] = useState([]);

  const appInfoSection = () => {
    return (
      <div className={style.appInfoSection}>
        <div className={style.card}>
          <div className={style.info}>
            <h5>
              Free online quote poster <br />
              maker and generator
            </h5>
            <p>
              Create quote post at scale with our <br />
              massive library of quotes and easy to <br />
              design beautiful templates.
            </p>
            <button>
              Free quote poster maker
              <svg
                width='15'
                height='15'
                viewBox='0 0 15 15'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
                style={{
                  marginLeft: '10px',
                }}
              >
                <path
                  d='M1.07227 7.5H12.6348M12.6348 7.5L8.88476 3.75M12.6348 7.5L8.88476 11.25'
                  stroke='white'
                  strokeWidth='1.5'
                  strokeLinecap='round'
                  strokeLinejoin='round'
                />
              </svg>
            </button>
            <div className={style.quote}>
              “Create marketing copies at scale <br />
              with our AI powered content generation”
            </div>
            <div className={style.profile}>
              <div className={style.profileImage} />
              <div className={style.profileInfo}>
                <span>Raj Sharma</span>
                <span>Founder, Abc Music</span>
              </div>
            </div>
          </div>
          <div className={style.showCase}>
            <Image src={QuoteBanner} alt='Failed to load image' />
          </div>
        </div>
      </div>
    );
  };

  const quoteOfTheDay = () => {
    return (
      <div className={style.quoteoftheday}>
        <span>{quotesdata.quotesdata[0].quote_of_the_day.quote}</span>
      </div>
    );
  };

  const famousAuthor = () => {
    return (
      <div className={style.famousAuthor}>
        <div className={style.authorHeading}>
          <h1>Explore quotes by famous authors</h1>
          <Link href='/apps/quotes/famousauthors' passHref>
            <button>See All</button>
          </Link>
        </div>
      </div>
    );
  };

  const popularTopics = () => {
    return (
      <div className={style.famousAuthor}>
        <div className={style.authorHeading}>
          <h1>Explore quotes by popular topics</h1>
          <button>See All</button>
        </div>
      </div>
    );
  };

  const hundredQuotes = () => {
    return (
      <div className={style.quoteoftheday}>
        <Image src={Rating} alt='Failed to load image' />
        <span>
          “Made by people who value financial well-being (like me) and care
          about citizens success and happiness.”
        </span>
      </div>
    );
  };

  // Need More Information
  const NeedMoreInformation = () => {
    return (
      <div className={style.fifthBlackSection}>
        <h1>
          You focus on telling stories,
          <br />
          we do everything else.
        </h1>
        <button>Signup for Free</button>
      </div>
    );
  };

  const onFormSubmit = async (e) => {
    e.preventDefault();
    const res = await axios.get(
      `https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/search?q=${search}`
    );

    setQuotes(res.data);
  };

  const handleChange = (event) => {
    setSearch(event.target.value);
  };

  const authors = [
    'A. P. J. Abdul Kalam',
    'Abraham Lincoln',
    'Albert Einstein',
    'Swami Vivekananda',
    'Elon Musk',
    'Winston Churchill',
    'Helen Keller',
    'Charles R. Swindoll',
    'David Amerland',
    'Nelson Mandela',
    'Aristotle',
    'Barack Obama',
    'Bill Gates',
    'Bruce Lee',
    'Buddha Confucius',
    'Dalai Lama',
    'Epictetus',
    'Ernest Hemingway',
    'Franklin D. Roosevelt',
    'Friedrich Nietzsche',
    'Henry Ford',
    'Isaac Newton',
    'Leonardo da Vinci',
    'Mahatma Gandhi',
    'Mark Twain',
    'Martin Luther King, Jr.',
    'Maya Angelou',
    'Mike Tyson',
    'Mother Teresa',
    'Muhammad Ali',
    'Oprah Winfrey',
    'Paulo Coelho',
    'Plato',
    'Rabindranath Tagore',
    'Robert Frost',
    'Socrates',
    'Steve Jobs',
    'Swami Sivananda',
    'Theodore Roosevelt',
    'Thomas A. Edison',
    'Walt Disney',
    'Warren Buffett',
    'William Shakespeare',
    'Zig Ziglar',
  ];

  const topic = [
    'Love',
    'Life',
    'Inspirational',
    'Humor',
    'Philosophy',
    'God',
    'Truth',
    'Wisdom',
    'Poetry',
    'Romance',
    'Death',
    'Happiness',
    'Hope',
    'Faith',
    'Writing',
    'Motivational',
    'Religion',
    'Relationships',
    'Spirituality',
    'Success',
    'Time',
    'Knowledge',
    'Science',
  ];

  const home = () => {
    return (
      <div>
        {appInfoSection()}
        {quoteOfTheDay()}
        <div className={style.seeAllQuotes}>
          <span>See all authors and topics</span>
          <div className={style.downArrow}>
            <Image src={DownArrow} alt='Down Arrow' />
          </div>
        </div>
        {famousAuthor()}
        <div className={style.gridContainer}>
          <div className={style.authorGrid}>
            {authors.map((data, index) => {
              const quoteSlug = slugify(data);
              return (
                <Link
                  href={{
                    pathname: '/apps/quotes/authors/[slug]',
                    query: data,
                  }}
                  as={`/apps/quotes/authors/${quoteSlug}-quotes`}
                  key={index}
                  passHref
                >
                  <div className={style.gridItem}>{data}</div>
                </Link>
              );
            })}
          </div>
        </div>
        {popularTopics()}
        <div className={style.gridContainer}>
          <div className={style.authorGrid}>
            {topic.map((data, index) => {
              const quoteSlug = slugify(data);
              return (
                <Link
                  href={{
                    pathname: '/apps/quotes/topics/[topic]',
                    query: data,
                  }}
                  as={`/apps/quotes/topics/${quoteSlug}-quotes`}
                  key={index}
                  passHref
                >
                  <div className={style.gridItem}>{data}</div>
                </Link>
              );
            })}
          </div>
        </div>
        {hundredQuotes()}
        <div className={style.seeAllQuotes}>
          <span>Read top 100 quotes</span>
          <div className={style.downArrow}>
            <Image src={DownArrow} alt='Down Arrow' />
          </div>
        </div>
      </div>
    );
  };

  const slugify = (string) => {
    return string
      .toString()
      .trim()
      .toLowerCase()
      .replace(/\s+/g, '-')
      .replace(/[^\w\-]+/g, '')
      .replace(/\-\-+/g, '-')
      .replace(/^-+/, '')
      .replace(/-+$/, '');
  };

  const copyText = (quote, author) => {
    toast('Copied!', {
      position: 'top-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    navigator.clipboard.writeText(quote + ' :-' + author);
  };

  const searchResults = () => {
    return (
      <div className={style.searchContainerCenter}>
        <div style={{ overflow: 'scroll' }}>
          <Masonry columnsCount={3} gutter='20'>
            {quotes.map((data, index) => {
              const fiveWords = data.Queote.split(' ').slice(0, 5).join(' ');
              const slug = slugify(fiveWords);

              let trimedAuthor = '';
              if (data.Author === undefined) {
                trimedAuthor = 'Unknown author';
              } else {
                if (data.Author.includes(',')) {
                  trimedAuthor = data.Author.substring(
                    0,
                    data.Author.indexOf(',')
                  );
                } else {
                  trimedAuthor = data.Author;
                }
              }

              const slugAuthor = slugify(trimedAuthor);
              const author = slugAuthor.substring(0, 30);
              return (
                <div key={data._id}>
                  <Link
                    href={{
                      pathname: '/apps/quotes/[slug]',
                      query: data,
                    }}
                    as={`/apps/quotes/${slug}-quote-by-${author}-${data._id}`}
                    passHref
                  >
                    <div className={style.card} style={{ cursor: 'pointer' }}>
                      <p>{data.Queote}</p>
                      <div className={style.authorContainer}>
                        <span>- {data.Author}</span>
                      </div>
                    </div>
                  </Link>
                  <div className={style.functions}>
                    <div className={style.img1}>
                      <Image
                        src={Save}
                        alt=''
                        onClick={() => {
                          toast('Subscribe to save quote.');
                        }}
                      />
                    </div>
                    <div className={style.img2}>
                      <Image
                        src={Copy}
                        alt=''
                        onClick={() => copyText(data.Queote, data.Author)}
                      />
                    </div>
                  </div>
                </div>
              );
            })}
          </Masonry>
        </div>
      </div>
    );
  };

  return (
    <div>
      <Navbar />
      <div className={style.header}>
        <h1>Get Inspired</h1>
        <form className={style.nosubmit} onSubmit={onFormSubmit}>
          <input
            className={style.nosubmit}
            type='search'
            placeholder='Search from 1 million quotes by keywords, authors, books and more'
            onChange={handleChange}
          />
        </form>
        {search !== '' ? null : (
          <div>
            <div className={style.headerImage}>
              <Image src={banner} alt='failed to load image' />
            </div>
            <div className={style.seeAll}>
              <span>See all authors and topics</span>
              <div className={style.downArrow}>
                <Image src={DownArrow} alt='Down Arrow' />
              </div>
            </div>
          </div>
        )}
      </div>
      {search !== '' ? searchResults() : home()}
      {NeedMoreInformation()}
      <Footer />
    </div>
  );
};

export default Quotes;
