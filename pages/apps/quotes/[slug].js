import React from 'react';
import { useRouter } from 'next/dist/client/router';
import Navbar from '../../../Components/LandingPageComponents/NavigationBar';
import style from '../../../styles/SingleQuote.module.css';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Link from 'next/link';
import Head from 'next/head';
import { getAuthorQuotes } from '../../../lib/getQuotesOfAuthor';
import Masonry from 'react-responsive-masonry';
import Save from '../../../public/Assets/Save.svg';
import Copy from '../../../public/Assets/Copy.svg';
import Image from 'next/image';
import { toast } from 'react-toastify';
import axios from 'axios';

export async function getServerSideProps(context) {
  let trimedAuthor = '';
  let quotes;
  let singlequote;
  if (context.query.Author !== undefined) {
    if (context.query.Author.includes(',')) {
      trimedAuthor = context.query.Author.substring(
        0,
        context.query.Author.indexOf(',')
      );
    } else {
      trimedAuthor = context.query.Author;
    }
    quotes = await getAuthorQuotes(trimedAuthor);
  } else {
    const id = context.query.slug.substring(context.query.slug.lastIndexOf("-")).replace(/-/g, " ").trim();
    const response = await axios.get(`https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/get/single/quote?search=${id}`)
    singlequote = response.data
    if (singlequote.Author.includes(',')) {
      trimedAuthor = singlequote.Author.substring(
        0,
        singlequote.Author.indexOf(',')
      );
    } else {
      trimedAuthor = singlequote.Author;
    }
    quotes = await getAuthorQuotes(trimedAuthor);
  }

  if (!singlequote) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      quotes,
      singlequote
    },
  };
}

const QuotePage = (props) => {
  const router = useRouter();
  const data = router.query.Author !== undefined ? router.query : props.singlequote
  let splittedTags = '';
  if (typeof data.Tags === "string") {
    splittedTags = data.Tags.split(',');
  } else {
    splittedTags = data.Tags.toString().split(',');
  }

  const Tags = Array.from(splittedTags);


  const slugify = (string) => {
    return string
      .toString()
      .trim()
      .toLowerCase()
      .replace(/\s+/g, '-')
      .replace(/[^\w\-]+/g, '')
      .replace(/\-\-+/g, '-')
      .replace(/^-+/, '')
      .replace(/-+$/, '');
  };


  const quote = () => {
    const author = slugify(data.Author.includes(",") ? data.Author.substring(0, data.Author.indexOf(',')) : data.Author);

    return (
      <div className={style.quoteoftheday}>
        <h1>{data.Queote}</h1>
        <Link
          href={{
            pathname: '/apps/quotes/authors/[slug]',
            query: data.Author.includes(",") ? data.Author.substring(0, data.Author.indexOf(',')) : data.Author,
          }}
          as={`/apps/quotes/authors/${author}-quotes`}
          passHref
        >
          <h2>{data.Author}</h2>
        </Link>
      </div>
    );
  };

  const relatedTopic = () => {
    return (
      <div className={style.gridContainer}>
        <div className={style.authorGrid}>
          {Tags.map((data, index) => {
            const quoteSlug = slugify(data);
            return (
              <Link
                href={{
                  pathname: '/apps/quotes/topics/[topic]',
                  query: data,
                }}
                as={`/apps/quotes/topics/${quoteSlug}-quotes`}
                key={index}
                passHref
              >
                <div className={style.gridItem}>{data}</div>
              </Link>
            );
          })}
        </div>
      </div>
    );
  };

  // Need More Information
  const NeedMoreInformation = () => {
    return (
      <div className={style.fifthBlackSection}>
        <h1>
          You focus on telling stories,
          <br />
          we do everything else.
        </h1>
        <button>Signup for Free</button>
      </div>
    );
  };

  const copyText = (quote, author) => {
    toast('Copied!', {
      position: 'top-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    navigator.clipboard.writeText(quote + ' :-' + author);
  };

  const moreQuotes = () => {
    return (
      <div className={style.searchContainerCenter}>
        <div style={{ overflow: 'scroll' }}>
          <Masonry columnsCount={3} gutter={20}>
            {props.quotes.map((data, index) => {
              const fiveWords = data.Queote.split(' ').slice(0, 5).join(' ');
              const slug = slugify(fiveWords);
              if (data.Author.includes(',')) {
                trimedAuthor = data.Author.substring(
                  0,
                  data.Author.indexOf(',')
                );
              } else {
                trimedAuthor = data.Author;
              }
              const slugAuthor = slugify(trimedAuthor);
              const author = slugAuthor.substring(0, 30);

              return (
                <div key={data._id}>
                  <Link
                    href={{
                      pathname: '/apps/quotes/[slug]',
                      query: data,
                    }}
                    as={`/apps/quotes/${slug}-quote-by-${author}-${data._id}`}
                    passHref
                  >
                    <div className={style.card} style={{ cursor: 'pointer' }}>
                      <p>{data.Queote}</p>
                      <div className={style.authorContainer}>
                        <span>- {data.Author}</span>
                      </div>
                    </div>
                  </Link>
                  <div className={style.functions}>
                    <div className={style.img1}>
                      <Image
                        src={Save}
                        alt=''
                        onClick={() => {
                          toast('Subscribe to save quote.');
                        }}
                      />
                    </div>
                    <div className={style.img2}>
                      <Image
                        src={Copy}
                        alt=''
                        onClick={() => copyText(data.Queote, data.Author)}
                      />
                    </div>
                  </div>
                </div>
              );
            })}
          </Masonry>
        </div>
      </div>
    );
  };

  const trimedQuote = data.Queote.substring(0, 98);
  const trimedTitle = data.Queote.substring(0, 35);

  let trimedAuthor = '';
  if (data.Author.includes(',')) {
    trimedAuthor = data.Author.substring(0, data.Author.indexOf(','));
  } else {
    trimedAuthor = data.Author;
  }

  const description =
    trimedQuote +
    '...' +
    trimedAuthor +
    ' Quotes ' +
    ' - Quotes by Creatosaurus';
  const title = trimedAuthor + ' - ' + trimedTitle + '...';

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name='description' content={description} />
        <meta property='og:title' content={title} key='title' />
        <meta
          property='og:description'
          content={description}
          key='description'
        />
        <meta property='og:site_name' content='Quotes by Creatosaurus'></meta>
        <meta
          property='og:url'
          content='https://www.creatosaurus.io/apps/quotes'
          key='url'
        />

        {/* Twitter */}
        <meta name='twitter:title' content={title} />
        <meta name='twitter:description' content={description} />

        <meta name='twitter:card' content='summary_large_image' />
        <meta name='twitter:site' content='@creatosaurushq' />
        <meta
          name='twitter:url'
          content='https://www.creatosaurus.io/apps/quotes'
        />
      </Head>
      <Navbar />
      <div className={style.headingButton}>
        <button>
          Free quote poster maker. Thousands of beautiful quote templates{' '}
          <svg
            width='16'
            height='15'
            viewBox='0 0 16 15'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
          >
            <g clipPath='url(#clip0_5605_1628)'>
              <path
                d='M1.57227 7.5H13.1348M13.1348 7.5L9.38476 3.75M13.1348 7.5L9.38476 11.25'
                stroke='white'
                strokeWidth='1.5'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
            </g>
            <defs>
              <clipPath id='clip0_5605_1628'>
                <rect
                  width='15'
                  height='15'
                  fill='white'
                  transform='translate(0.5)'
                />
              </clipPath>
            </defs>
          </svg>
        </button>
      </div>
      {quote()}
      <div className={style.relatedTopic}>
        <h3>Related topics</h3>
      </div>
      {relatedTopic()}
      <div className={style.relatedTopic}>
        <h2>More Quotes by {trimedAuthor}</h2>
      </div>

      <div className={style.moreQuotes}>{moreQuotes()}</div>
      {NeedMoreInformation()}
      <Footer />
    </div>
  );
};

export default QuotePage;
