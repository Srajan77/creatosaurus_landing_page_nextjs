import React from 'react'
import Head from 'next/head';
import Auth from '../Components/Auth/Auth'

const login = () => {
    return (
        <div style={{ backgroundColor: '#f8f8f9', height: '100vh' }}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>Creatosaurus | Login</title>
            </Head>
            <Auth title="Login" />
        </div>
    )
}

export default login