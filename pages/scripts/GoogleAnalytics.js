import React from 'react'
import Script from 'next/script';

const GoogleAnalytics = () => {
    return (
        <>
            <Script strategy='afterInteractive'
                src={`https://www.googletagmanager.com/gtag/js?id=G-B80P5FRDJJ`}
            />
            <Script strategy='afterInteractive'>
                {
                    `
           window.dataLayer = window.dataLayer || [];
           function gtag(){dataLayer.push(arguments);}
           gtag('js', new Date());
           gtag('config', 'G-B80P5FRDJJ'); 
           `
                }
            </Script>
        </>
    );
}

export default GoogleAnalytics

