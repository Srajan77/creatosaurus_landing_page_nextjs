module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['ahijftavqd.creatosaurus.io', 'images.unsplash.com', 'static.ghost.org', 'd3u00cz8rshep5.cloudfront.net'],
  },
};
